package mab.util;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NormalCumulativeProbabilityTest {
    @Test
    void broadAccuracyTest() {
        NormalDistribution[] normals = {
                new NormalDistribution(null, 0.7242688517832356, 0.174951807910961880),
                new NormalDistribution(null, 0.7369891606922598, 0.15061303957684957),
                new NormalDistribution(null, 0.9401560978046573, 0.2269214690531064),
                new NormalDistribution(null, 0.4121120393953186, 0.7642008342910613),
                new NormalDistribution(null, 0.4434511492169605, 0.031100895919553513)
        };
        int index = 0;
        double maxDifference = 0, maxDifferenceX = Double.NaN;
        int maxIndex = -1;
        for (double x = 0; x < 1; x += 0.01) {
            for (int j = 0; j < normals.length; j++) {
                if (j != index) {
                    double precise = normals[j].cumulativeProbability(x);
                    double fast = NormalCumulativeProbability.eval(
                            normals[j].getMean(), normals[j].getStandardDeviation(), x);
                    double difference = precise - fast;
                    if (Math.abs(difference) > Math.abs(maxDifference)) {
                        maxDifference = difference;
                        maxDifferenceX = x;
                        maxIndex = j;
                    }
                }
            }
        }
        // System.err.println("MaxDifference: " + maxDifference + " for x = " + maxDifferenceX + " and index = " + maxIndex);
        assertEquals(0, maxDifference, 0.001);
    }

    @Test
    void specificCaseTest() {
        NormalDistribution dis = new NormalDistribution(null, 0.9401560978046573, 0.2269214690531064);
        double x = 0.94;
        double precise = dis.cumulativeProbability(x);
        double fast = NormalCumulativeProbability.eval(dis.getMean(), dis.getStandardDeviation(), x);
        // System.err.println("precise: " + precise + "\t fast: " + fast);
        assertEquals(precise, fast, 0.001);
    }

    @Test
    void standardNormalTest() {
        NormalDistribution dis = new NormalDistribution(null, 0, 1);
        double maxDifference = 0, maxDifferenceX = Double.NaN;
        for (double x = 0; x < 1; x += 0.01) {
            double precise = dis.cumulativeProbability(x);
            double fast = NormalCumulativeProbability.eval(
                    dis.getMean(), dis.getStandardDeviation(), x);
            double difference = precise - fast;
            if (Math.abs(difference) > Math.abs(maxDifference)) {
                maxDifference = difference;
                maxDifferenceX = x;
            }
        }
        // System.err.println("MaxDifference: " + maxDifference + " for x = " + maxDifferenceX);
        assertEquals(0, maxDifference, 0.001);
    }
}