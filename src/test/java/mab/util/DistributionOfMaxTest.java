package mab.util;

import integrate.Integrator;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DistributionOfMaxTest {
    private DistributionOfMax x;

    @BeforeEach
    void setUp() {
        x = new DistributionOfMax(Integrator.Trapezoid);
    }

    @Test
    void testTwoArms() {
        NormalDistribution[] normals = { new NormalDistribution(null, 0, 1),
                                         new NormalDistribution(null, 10, 1)};
        double[] distrib = x.get(normals);
        // System.err.println(Arrays.toString(distrib));
        assertEquals(distrib.length, 2);
        assertEquals(0, distrib[0], 0.01 );
        assertEquals(1, distrib[1], 0.01 );
    }

    @Test
    void testFiveArms() {
        NormalDistribution[] normals = {
                new NormalDistribution(null, 0.7242688517832356, 0.174951807910961880),
                new NormalDistribution(null, 0.7369891606922598,0.15061303957684957),
                new NormalDistribution(null,0.9401560978046573,	0.2269214690531064),
                new NormalDistribution(null,0.4121120393953186,0.7642008342910613),
                new NormalDistribution(null,0.4434511492169605,0.031100895919553513)
        };
        double[] distrib = x.get(normals);
        System.err.println(Arrays.toString(distrib));
        double tot = Arrays.stream(distrib).sum();
        assertEquals(distrib.length, 5);
        assertEquals(1, tot, 0.01 );
    }

    @Test
    void testNullVariance() {
        NormalDistribution[] normals = {
                new NormalDistribution(null, 0.7242688517832356, 0.174951807910961880),
                null,
                new NormalDistribution(null,0.9401560978046573,	0.2269214690531064),
                new NormalDistribution(null,0.4121120393953186,0.7642008342910613),
                new NormalDistribution(null,0.4434511492169605,0.031100895919553513)
        };
        double[] meansWithNoVariance = { 0, 1, 0, 0, 0};
        double[] distrib = x.get(normals, List.of(1), meansWithNoVariance);
        System.err.println(Arrays.toString(distrib));
        double tot = Arrays.stream(distrib).sum();
        assertEquals(distrib.length, 5);
        assertEquals(1, tot, 0.01 );
    }

    @Test
    void testNullVariance2() {
        NormalDistribution[] normals = {
                null,
                new NormalDistribution(null,0.7420114756522606,	0.1549380065176447),
                null,
                null,
                new NormalDistribution(null,0.16840304469951814, 0.18384394692124126)
        };
        double[] meansWithNoVariance = { 0.9843185637858738, 0, 1.2026472870219553, 0.7954995852634779, 0};
        double[] distrib = x.get(normals, List.of(0, 2, 3), meansWithNoVariance);
        System.err.println(Arrays.toString(distrib));
        double tot = Arrays.stream(distrib).sum();
        assertEquals(distrib.length, 5);
        assertEquals(1, tot, 0.01 );
    }
}