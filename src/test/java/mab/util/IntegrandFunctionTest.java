package mab.util;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegrandFunctionTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    void testFiveArms() {
        NormalDistribution[] normals = {
                new NormalDistribution(null, 0.7242688517832356, 0.174951807910961880),
                new NormalDistribution(null, 0.7369891606922598,0.15061303957684957),
                new NormalDistribution(null,0.9401560978046573,	0.2269214690531064),
                new NormalDistribution(null,0.4121120393953186,0.7642008342910613),
                new NormalDistribution(null,0.4434511492169605,0.031100895919553513)
        };
        for (int i=0; i<normals.length; i++) {
            UnivariateFunction fast = DistributionOfMax.makeFastIntegrand(normals, i);
            UnivariateFunction precise = DistributionOfMax.makePreciseIntegrand(normals, i);
            double maxDifference = 0, maxDifferenceX = Double.NaN;
            for (double x = 0; x<1; x+=0.01) {
                double fastVal = fast.value(x);
                double preciseVal = precise.value(x);
                double difference = preciseVal - fastVal;
                if (Math.abs(difference) > Math.abs(maxDifference)) {
                    maxDifference = difference;
                    maxDifferenceX = x;
                }
            }
            // System.err.println("MaxDifference: " + maxDifference + " for x = " + maxDifferenceX + " and index = " + i);
            assertEquals(0, maxDifference, 0.001);
        }
    }

}