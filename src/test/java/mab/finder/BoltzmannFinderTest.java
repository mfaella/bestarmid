package mab.finder;

import integrate.*;
import mab.MaxFinder;
import mab.finder.*;
import mab.sampler.*;

import mab.util.FinderComparison;
import mab.util.Moments;
import mab.util.NormalCumulativeProbability;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;

class BoltzmannFinderTest {
    //private EntropyMaxFinder finder;

    @BeforeEach
    void setUp() {

    }

    @Test
    void testTwoArms() {
		NormalDistribution[] normals = { new NormalDistribution(0, 200),
				                 new NormalDistribution(10, 10) };
		List<Moments> groundTruth = List.of(new Moments(0, 20), new Moments(1, 1));
		List<Function> functions = new ArrayList<>();
		List<Sampler> samplers = new ArrayList<>();
		makeInputs(normals, functions, samplers);

		MaxFinder[] finders = new MaxFinder[] {
		    new Uniform(),
		    new Proportional(10),
		    new FastProportional(10),
		    SmartMaxFinder.newFixedBudgetMCMC(2),
		    new Boltzmann2MaxFinder(1),
		    new FixedAllocationMaxFinder(new int[] { 200, 10 })
		};

		FinderComparison comp = new FinderComparison(finders); 
		comp.noDetailedOutput();

		final int budget = 210;
		for (int i=0; i<1_000; i++) {
		    /*		    Supplier<MaxFinder> boltz = () -> new BoltzmannMaxFinder(samplers, functions, budget, 20, 1);
		    Supplier<MaxFinder> uniform = () -> new SimpleMaxFinder(samplers, functions, budget);
		    Supplier<MaxFinder> proportional = () ->
			new ProportionalMaxFinder(samplers, functions, budget, 10);
		    Supplier<MaxFinder> fastprop = () ->
			new ProportionalMaxFinder(samplers, functions, budget, 10);
		    Supplier<MaxFinder> vbr = () ->
			SmartMaxFinder.newFixedBudgetIid(samplers, functions, budget, 2);
		    Supplier<MaxFinder> opt = () -> new FixedAllocationMaxFinder(samplers, functions, new int[] { 200, 10 });
		    */
		    comp.addComparison(samplers, budget, 1, 1, "ciao");
		}
		System.err.println(comp);
		System.err.println(probOfError(new double[] {1, 0}, new double[] {1, 20}, new int[] {10, 200}));
		// System.err.println(probOfError(new double[] {1, 0}, new double[] {1, 20}, new int[] {20, 190}));
		assertEquals(0, 0);
	}

	private static double probOfError(double[] mean, double[] stdev, int[] allocation) {
		double differenceStdev = Math.sqrt(stdev[0]*stdev[0]/(double)allocation[0] + stdev[1]*stdev[1]/(double)allocation[1]);
		return NormalCumulativeProbability.eval(mean[0]-mean[1], differenceStdev, 0);
	}

	private static final Function identity = Function.make(a -> a[0], 1);

	public static void makeInputs(RealDistribution[] distrib, List<Function> functions, List<Sampler> samplers)
	{
		for (int i=0; i<distrib.length; i++) {
			samplers.add(new ApacheSampler(distrib[i]));
			functions.add(identity);
		}
	}

}
