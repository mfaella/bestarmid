package mab.finder;

import mab.MaxFinder;
import mab.sampler.ApacheSampler;
import mab.sampler.Sampler;

import mab.util.FinderComparison;
import mab.util.Moments;
import mab.util.NormalCumulativeProbability;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class EntropyFinderTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    void testTwoArms() {
		NormalDistribution[] normals = { new NormalDistribution(0, 20),
				                 new NormalDistribution(1, 1) };
		List<Moments> groundTruth = List.of(new Moments(0, 20), new Moments(1, 1));
		int maxId = 1;
		double maxMean = 1;
		List<Sampler> samplers = new ArrayList<>();
		makeInputs(normals, samplers);

		final int budget = 210;
		MaxFinder[] finders = {
				new FixedAllocationMaxFinder().setAllocation(new int[] { 200, 10 }),
				new AdaptiveUCBE(1.0),
				new Uniform(),
				SmartMaxFinder.newFixedBudgetIid(2),
				new FastProportional(20),
				new Entropy(),
		};
		FinderComparison comp = new FinderComparison(finders);
		comp.noDetailedOutput();

		for (int i=0; i<1_000; i++) {
			comp.addComparison(samplers, budget, maxId, maxMean, "");

/*			Supplier<MaxFinder> entropy = () -> new Entropy(samplers, functions, budget);
			Supplier<MaxFinder> uniform = () -> new Uniform(samplers, functions, budget);
			Supplier<MaxFinder> proportional = () ->
					new Proportional(samplers, functions, budget, 10);
			Supplier<MaxFinder> vbr = () ->
					SmartMaxFinder.newFixedBudgetIid(samplers, functions, budget, 2);
			Supplier<MaxFinder> opt = () -> new FixedAllocationMaxFinder(samplers, functions, new int[] { 200, 10 });
			Supplier<MaxFinder> opt2 = () -> new FixedAllocationMaxFinder(samplers, functions, new int[] { 190, 20 });
			Supplier<MaxFinder> opt3 = () -> new FixedAllocationMaxFinder(samplers, functions, new int[] { 180, 30 });
			Supplier<MaxFinder> cheat = () -> new CheatingMaxFinder(samplers, functions, groundTruth, budget);

			comp.addComparison(1, 1, "ciao", uniform, proportional, entropy, opt, cheat);

 */
		}
		System.err.println(comp);
		System.err.println(probOfError(new double[] {1, 0}, new double[] {1, 20}, new int[] {10, 200}));
		// System.err.println(probOfError(new double[] {1, 0}, new double[] {1, 20}, new int[] {20, 190}));
		assertEquals(0, 0);
	}

	private static double probOfError(double[] mean, double[] stdev, int[] allocation) {
		double differenceStdev = Math.sqrt(stdev[0]*stdev[0]/(double)allocation[0] + stdev[1]*stdev[1]/(double)allocation[1]);
		return NormalCumulativeProbability.eval(mean[0]-mean[1], differenceStdev, 0);
	}


	public static void makeInputs(RealDistribution[] distrib, List<Sampler> samplers)
	{
		for (int i=0; i<distrib.length; i++) {
			samplers.add(new ApacheSampler(distrib[i]));
		}
	}

}
