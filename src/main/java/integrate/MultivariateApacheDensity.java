package integrate;

import java.util.List;
import java.util.Arrays;

import mab.sampler.Sampler;
import org.apache.commons.math3.distribution.MultivariateRealDistribution;

/** A density based on a standard distribution.
    It can be directly sampled. */
public class MultivariateApacheDensity extends Density { //core
    private final MultivariateRealDistribution distrib;
    
    public MultivariateApacheDensity(MultivariateRealDistribution distrib,
									 double[] lowerBound, double[] upperBound) {
	super(lowerBound, upperBound);
	this.distrib = distrib;
        sanityChecks();
    }

    @Override
    public double eval(double[] point) {
	return distrib.density(point);
    }

    @Override
    public Sampler sampler() {
	return new Sampler() {
	    @Override
	    public double[] sample() {
		return distrib.sample();
	    }

	    @Override
	    public List<double[]> sample(int sampleCount) {
		return Arrays.asList(distrib.sample(sampleCount));
	    }
	};
    }

    private void sanityChecks() {
        if (distrib.getDimension() != dim())
            throw new IllegalArgumentException(
	    "Dimension incompatibility between distribution and bounds.");
    }
}
