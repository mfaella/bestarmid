package integrate;

/** The result of an integration of the type d(x)*f(x)dx,
    where d is a distribution.

    Besides the estimated value of the integral,
    it includes various data about the estimation and its expected error.
*/
public class Summary {
    
    public double value, // The estimate (public info)
    // variance,           // The variance of the estimate (public info)
	standardError,      // The standard deviation of the estimate (public info)
	iidStandardError,   // The standard deviation of the sample values (public info)
    // all other fields are private information used for variance refinement
        sumOfBatchAverages,        
        sumOfSquaredBatchAverages,
	sumOfValues,        // The sum of all the samples used to obtain this result
	sumOfSquaredValues; // The sum of all the squared samples used to obtain this result

    public int sampleCount, // The number of samples used to obtain this result
        batchLength,           // The length of the batches used to compute the variance
        batchCount;            // The number of the batches used to compute the variance
    public double[] lastSample;
    
    // /** Construction is reserved to this package and possibly to subclasses. */
    public Summary(double value) {
	this.value = value;
    }

    /** Returns the estimated value of this integral. */
    public double getValue() {
	return value;
    }

    /** Returns the standard error of this estimate assuming correlated samples.
    	Computed using non-overlapping batch means.
    */
    public double getStandardError() {
        return standardError;
    }

    /** Returns the standard error of this estimate assuming independent samples.
    	Computed using sample variance.
    */
    public double getIidStandardError() {
        return iidStandardError;
    }

    /** Returns the number of samples used to obtain this result. */
    public int getSampleCount() {
	return sampleCount;
    }

    public int getBatchLength() { //mai usato
	return batchLength;
    }

    public String toString() {
	return "" + value;
    }
}
