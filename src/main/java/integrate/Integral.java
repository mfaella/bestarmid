package integrate;

import mab.Globals;

import java.util.List;
import java.util.function.ToDoubleFunction;

public class Integral {

	/* Trade-offs involved in this choice:
	 *   the value 0.5 is optimal for a given number of samples,
	 *   but the batch length is fixed during refinements,
	 *   so we start with a somewhat larger batch length, to get better
	 *   accuracy in the long run.
	 */
	private static final double BATCH_LENGTH_EXPONENT = 0.6;
	private static final int MIN_BATCH_LENGTH = 20;
	private static final boolean DEBUG = false;

	/** Compute an estimate of the integral of integrand(x) * density(x)
	 using the given number of samples.
	 */
	public static Summary compute(Function integrand, Density density, int sampleCount) { //mai usato
		if (sampleCount<=0)
			throw new IllegalArgumentException("Cannot estimate integral with no samples.");

		List<double[]> samples = density.sampler().sample(sampleCount);
		return compute(integrand, samples);
	}

	public static <T> double compute(ToDoubleFunction<T> integrand, List<T> samples) { //mai usato
		double total = 0;
		for (T sample: samples) {
			total += integrand.applyAsDouble(sample);
		}
		return total / samples.size();
	}


	/** Compute an estimate of the integral of integrand(x) * density(x),
	 given a list of samples distributed like the density.
	 */
	public static Summary compute(Function integrand, List<double[]> samples) {
		if (samples.isEmpty())
			throw new IllegalArgumentException("Cannot estimate integral with no samples.");
		return refine(null, integrand, samples);
	}


	/** Refines a given estimate of the integral of integrand(x) * density(x),
	 given a previous estimate, the integrand, and a list of additional samples
	 distributed like the density.

	 If you don't have a previous estimate, use {@code null} as first argument.

	 @param oldResult A previous result for the same integral. Can be {@code null}.
	 */
	public static Summary refine(Summary oldResult, Function integrand, List<double[]> additionalSamples) {
		if (additionalSamples.isEmpty())
			return oldResult;

		if (oldResult == null) {
			oldResult = new Summary(0);
			// all other fields are left to their zero default
			oldResult.batchLength = (int) Math.pow(additionalSamples.size(), BATCH_LENGTH_EXPONENT);
			if (oldResult.batchLength < MIN_BATCH_LENGTH)
				oldResult.batchLength = MIN_BATCH_LENGTH;
		}

		final int newSampleCount = additionalSamples.size();
		final int batchLength = oldResult.batchLength;

		if (batchLength<=0)
			throw new IllegalArgumentException("Cannot refine a result with illegal batch length.");

		// Discard the last incomplete batch
		final int batchCount = newSampleCount / batchLength;

		// DEBUG
		// System.out.println(newSampleCount + "\t" + batchLength + "\t" + batchCount);

		double batchAverages[] = new double[batchCount];
		int i = 0, batch = 0;
		// For value variance:
		double sumOfValues = 0, sumOfSquaredValues = 0;

		for (double[] point: additionalSamples) {
			double value = integrand.eval(point);
			sumOfValues += value;
			sumOfSquaredValues += value * value;

			// slower but possibly more precise
			// average = average * (i/(double)(i+1)) + value/(i+1);
			if (batch < batchCount)
				batchAverages[batch] += value;
			i++;
			if (i % batchLength == 0) {
				batchAverages[batch] /= batchLength;
				batch++;
			}
		}
		// Estimate variance using the nonoverlapping batch means method
		// See Handbook of MCMC, sec. 1.10.1.
		double sumOfBatchAverages = 0, sumOfSquaredBatchAverages = 0;
		for (double batchAverage: batchAverages) {
			sumOfBatchAverages += batchAverage;
			sumOfSquaredBatchAverages += batchAverage * batchAverage;
		}

		// Combining old result and new one
		// It works even for an empty old result (all zeros) coming from the "compute" method
		int oldSampleCount   = oldResult.sampleCount,
				grandSampleCount = oldSampleCount + newSampleCount,
				grandBatchCount  = oldResult.batchCount + batchCount;
		double grandAverage =
				(oldResult.sumOfValues + sumOfValues) / (double) grandSampleCount;

		double oldContribution = oldResult.sumOfSquaredBatchAverages
				- 2 * grandAverage * oldResult.sumOfBatchAverages
				+ oldResult.batchCount * grandAverage * grandAverage;
		double newContribution = sumOfSquaredBatchAverages
				- 2 * grandAverage * sumOfBatchAverages
				+ batchCount * grandAverage * grandAverage;
		double grandVariance = batchLength * (oldContribution + newContribution) /
				(double) (grandBatchCount * grandSampleCount);
		// System.out.println("oC " + oldContribution + "\t nC " + newContribution + "\t oSC " + oldSampleCount);

		if (DEBUG) {
			System.out.println("oSC: " + oldSampleCount + "\t nSC: " + newSampleCount);
			System.out.println("oSOV: " + oldResult.sumOfValues + "\t nSOV: " + sumOfValues);
			System.out.println("oSOS: " + oldResult.sumOfSquaredValues + "\t nSOS: " + sumOfSquaredValues);
		}

		// Bessel's correction
		int besselDenumerator = grandSampleCount;
		if (besselDenumerator > 1)
			besselDenumerator--;

		double grandValueVariance =
				( grandAverage * grandAverage * grandSampleCount
						+ oldResult.sumOfSquaredValues + sumOfSquaredValues
						- 2 * grandAverage * ( oldResult.sumOfValues + sumOfValues) )
						/ (double) (besselDenumerator);

		// Fix numerical errors when observed variance is close to zero
		if (grandValueVariance < 0)
			grandValueVariance = 0;

		if (DEBUG)
			System.out.println("grandAverage: " + grandAverage + "\t grandValueVariance: " + grandValueVariance);

		// Here it becomes a squared standard error
		grandValueVariance /= grandSampleCount;

		Summary newResult = new Summary(grandAverage);
		newResult.sampleCount = grandSampleCount;

		// Standard error based on non-overlapping batch means
		newResult.standardError = Math.sqrt(grandVariance);
		newResult.batchLength = batchLength;
		newResult.batchCount = grandBatchCount;
		newResult.sumOfBatchAverages = oldResult.sumOfBatchAverages + sumOfBatchAverages;
		newResult.sumOfSquaredBatchAverages = oldResult.sumOfSquaredBatchAverages + sumOfSquaredBatchAverages;

		// I.i.d. standard error (assuming independent samples)
		newResult.iidStandardError = Math.sqrt(grandValueVariance);
		newResult.sumOfValues = oldResult.sumOfValues + sumOfValues;
		newResult.sumOfSquaredValues = oldResult.sumOfSquaredValues + sumOfSquaredValues;

		if (DEBUG)
			System.out.println("batch means SE: " + newResult.standardError +
					"\tiid SE: " + newResult.iidStandardError +
					"\t sample count: " + newResult.sampleCount);

		newResult.lastSample = additionalSamples.get(newSampleCount-1);
		return newResult;
	}
}
