package integrate;

import mab.sampler.Metropolis;
import mab.sampler.Sampler;

/** A density based on a custom function.
    It must be sampled using an MCMC procedure. */
public class CustomDensity extends Density {  //core
    private final Function function;
    
    public CustomDensity(Function density, double[] lowerBound, double[] upperBound) {
	super(lowerBound, upperBound);
	this.function = density;
        sanityChecks();
    }

    @Override
    public double eval(double[] point) {
	return function.eval(point);
    }

    @Override
    public Sampler sampler() {
	return new Metropolis(this);
    }
    
    private void sanityChecks() {
        if (function.arity() != dim())
            throw new IllegalArgumentException(
	    "Dimension incompatibility between function and bounds.");
    }
}
