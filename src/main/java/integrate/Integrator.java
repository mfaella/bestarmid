package integrate;

import org.apache.commons.math3.analysis.integration.*;

public enum Integrator {
    Simpson() {
        @Override
        public UnivariateIntegrator make (double relativeAccuracy, double absoluteAccuracy){
	    return new SimpsonIntegrator(relativeAccuracy, absoluteAccuracy, 3, 64 );
	    // return new SimpsonIntegrator();
        }
    },
    Legendre1() {
        @Override
        public UnivariateIntegrator make (double relativeAccuracy, double absoluteAccuracy){
            return new IterativeLegendreGaussIntegrator(1, relativeAccuracy, absoluteAccuracy);
        }
    },
    Legendre5() {
        @Override
        public UnivariateIntegrator make (double relativeAccuracy, double absoluteAccuracy){
            return new IterativeLegendreGaussIntegrator(5, relativeAccuracy, absoluteAccuracy);
        }
    },
    Romberg() {
        @Override
        public UnivariateIntegrator make (double relativeAccuracy, double absoluteAccuracy){
            return new RombergIntegrator(relativeAccuracy, absoluteAccuracy, 3, 32);
        }
    },
    Trapezoid() {
        @Override
        public UnivariateIntegrator make (double relativeAccuracy, double absoluteAccuracy){
            return new TrapezoidIntegrator(relativeAccuracy, absoluteAccuracy, 3, 64);
        }
    };
    public abstract UnivariateIntegrator make(double relativeAccuracy, double absoluteAccuracy);
}
