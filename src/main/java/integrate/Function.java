package integrate;

/** A multi-variate real function.
 * 
 */
public interface Function { //core
    double eval(double[] args);
    int arity();

    public interface Map {
	double f(double[] args);
    }
   
    /** Factory method for lambda-expressions. Example usage:
        {@code Function foo = Function.make(args -> args[0] + 4 * args[1], 2)}
     */
    public static Function make(Map function, int n) {
	return new Function() {
	    public double eval(double[] args) { return function.f(args); }
	    public int arity() { return n; }
	};
    }
}

	
