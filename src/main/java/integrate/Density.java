package integrate;

import mab.sampler.Sampleable;

/** A multivariate probability distribution over a hyper-rectangle. */
public abstract class Density implements Sampleable { //core
    public final double[] lowerBound, upperBound;
    double groundTruth; // optional value for validation experiments

    public Density(double[] lowerBound, double[] upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        sanityChecks();
    }

    public abstract double eval(double[] point);

    /** Returns the middle point of the feasible region. */
    public double[] witness() {
	final int dim = dim();
        double[] result = new double[dim];
        for (int i=0; i<dim; i++)
            result[i] = (upperBound[i] + lowerBound[i])/2;
        return result;
    }
    
    public final int dim() {
	return lowerBound.length;
    }
    
    public final boolean isFeasible(double[] point) {
	for (int i=0; i<dim(); i++)
	    if (point[i]<lowerBound[i] || point[i]>upperBound[i])
		return false;
	return true;
    }

    private void sanityChecks() {
        final int dim = dim();
	if (dim<1)
            throw new IllegalArgumentException(
	    "Dimension of the density must be at least 1.");
        if (upperBound.length != dim)
            throw new IllegalArgumentException(
	    "Dimension incompatibility between lower and upper bounds.");
        for (int i=0; i<dim; i++)
            if (lowerBound[i] >= upperBound[i])
                throw new IllegalArgumentException(
                "Upper bound is not greater than lower bound at dimension " + i + "");
    }
}
