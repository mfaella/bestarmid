package mab.sampler;

import java.util.List;

public interface Sampler {
    
    /** 
     * Returns a new sample.
     * @return a new sample
     */
    double[] sample();

    
    /** 
     * Returns a list of new samples of length {@code sampleCount}.
     * @return a list of new samples of length {@code sampleCount}
     */
    List<double[]> sample(int sampleCount);
}
