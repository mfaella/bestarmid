package mab.sampler;

import integrate.Density;

import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class Metropolis implements Sampler { //sampler

    private Density density;
    private JumpFunction jump;
    private double[] latestSample;
    private static final Random random = new Random();

    /** Creates a Metropolis sampler based on a probability density.
     */
    public Metropolis(Density density) { //costruttore utilizzato solo in classe mai usata
	this.density = density;
	this.jump = new UniformJumpFunction(density);
    }

    /** 
     * Returns a list of new samples of length {@code sampleCount}.
     */
    public List<double[]> sample(int sampleCount) {
	if (sampleCount<=0)
	    throw new IllegalArgumentException("Must request at least one sample.");

	List<double[]> samples = new ArrayList<>(sampleCount);
	if (latestSample==null) {
	    initialize();
	    samples.add(latestSample);
	    continueSampling(samples, sampleCount-1);		    
	} else {
	    continueSampling(samples, sampleCount);		    
	}
        assert samples.size() == sampleCount;
        return samples;
    }

    private void sanityCheck(String msg) { //mai usata
	if (latestSample!=null && density.eval(latestSample)==0) {
	    System.out.println(" error: " + msg + " " + Arrays.toString(latestSample));
	    throw new Error();
	}
    }
    
    /** 
     * Returns a new sample. 
     */
    public double[] sample() {
	if (latestSample==null) {
	    initialize();
	} else {
	    List<double[]> samples = new ArrayList<>(1);
	    continueSampling(samples, 1);
	    latestSample = samples.get(0);
	    // sanityCheck("sample()");
	}
	return latestSample;
    }

    private static final int MAXITER = 1000;
    private void initialize() {
	double[] currentPoint = density.witness();
	int i = 0;
	while (!density.isFeasible(currentPoint) || density.eval(currentPoint)==0.0) {
	    currentPoint = jump.nextPoint(currentPoint);
	    i++;
	    if (i==MAXITER)
		throw new RuntimeException("Can't find a feasible point.");
	}
	latestSample = currentPoint;
	// sanityCheck("initialize()");
    }
    
    /* 
     * Adds {@code sampleCount} samples 
     * from the specified density to the specified list,
     * starting from and excluding the specified sample.
     */
    private void continueSampling(List<double[]> samples,
                                  int sampleCount) {
	assert latestSample != null;
	// sanityCheck("-- continueSampling()");
	
        // First sample is not added to result
        double[] currentPoint = latestSample;
        double currentValue = density.eval(currentPoint);

        // Other samples
	for (int i=0; i<sampleCount; i++) {

	    double[] nextPoint = jump.nextPoint(currentPoint);
	    assert density.isFeasible(nextPoint);   
	    double nextValue = density.eval(nextPoint);
	    
	    // Filter
	    double ratio = nextValue/currentValue;

	    if (currentValue == 0) 
		System.out.println("current: " + currentValue + "\t next: " + nextValue);

	    if (ratio>=1) { // Approved
		currentPoint = nextPoint;
		currentValue = nextValue;
	    } else {
		// We may save a few nanoseconds with a float
		float threshold = random.nextFloat();
		if (ratio > threshold) { // Approved

		    if (nextValue == 0) 
			System.out.println("current: " + currentValue + "\t next: " + nextValue);
		    
		    currentPoint = nextPoint;
		    currentValue = nextValue;
		}
		// Rejected: no update
	    }
            samples.add(currentPoint);
	    // System.out.println(i + ": \t" + Arrays.toString(currentPoint) + "\t : " + currentValue + " (" + integral + ")");
	}

	latestSample = currentPoint;	
	// sanityCheck("end of continueS");
    }
}
