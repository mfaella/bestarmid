package mab.sampler;

import integrate.Density;
import org.apache.commons.math3.distribution.*;

/** A multi-variate truncated gaussian jump distribution, 
 *  with independent dimensions (diagonal covariance).
 *
 *  WARNING: this jump is asymmetric.
 */
public class GaussianJumpFunction implements JumpFunction {  //sampler

    private final int dim;
    private final RealDistribution distrib[];
    private final Density density;
    
    // The number of default jumps needed to cross the integration space.
    private static final double DEFAULT_WIDTH = 8;

    /** Returns the default jump function for the specified density.
     *
     *  The default jump function is an independent Gaussian distribution 
     *  on each dimension, whose mean is the current point and whose variance is
     *  the width of the integration space (ub[i]-lb[i]) divided by DEFAULT_WIDTH. 
     */
    public GaussianJumpFunction(Density density) { //mai usata, è usata invece UniformJumpFunction
	this.dim = density.dim();
	this.density = density;
        double variance;

	distrib = new RealDistribution[dim];
        for (int i=0; i<dim; i++) {
            variance = (density.upperBound[i] - density.lowerBound[i]) / DEFAULT_WIDTH;
	    distrib[i] = new NormalDistribution(0, variance);
	}
    }
    
    public double[] nextPoint(double[] currentPoint) {
	double[] result = new double[dim];

	do {
	    for (int i=0; i<dim; i++) {
		result[i] = currentPoint[i] + distrib[i].sample();
	    }
	} while (!density.isFeasible(result));

	return result;
    }

     
}
