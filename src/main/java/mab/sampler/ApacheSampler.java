package mab.sampler;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.IntFunction;

import org.apache.commons.math3.distribution.*;


// Convert univariate and multivariate normal distributions to samplers
public class ApacheSampler implements Sampler {

	private IntFunction<List<double[]>> sampler;

	public ApacheSampler(RealDistribution d) {
		sampler = sampleCount -> {
			ArrayList<double[]> result = new ArrayList<>(sampleCount);
			double[] samples = d.sample(sampleCount);
			for (double sample: samples)
				result.add(new double[] { sample });
			return result;
		};
	}

	public ApacheSampler(MultivariateRealDistribution d) { //mai utilizzata
		sampler = sampleCount -> {
			double[][] samples = d.sample(sampleCount);
			return Arrays.asList(samples);
		};
	}

	public List<double[]> sample(int sampleCount) {
		return sampler.apply(sampleCount);
	}

	public double[] sample() {
		List<double[]> samples = sampler.apply(1);
		return samples.get(0);
	}
}
