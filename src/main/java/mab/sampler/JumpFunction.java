package mab.sampler;

public interface JumpFunction {  //sampler
    double[] nextPoint(double[] currentPoint);
}
