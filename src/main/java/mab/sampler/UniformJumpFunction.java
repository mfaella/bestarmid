package mab.sampler;

import integrate.Density;

import java.util.Random;

/** Jumps uniformly in the feasible hyper-rectangle of a given density.
    The next point does not depend on the previous one.
  */
public class UniformJumpFunction implements JumpFunction {  //sampler

    private static final Random random = new Random();
    private final int dim;
    private final double[] width;
    private final Density density;
    
    public UniformJumpFunction(Density density) {
	this.density = density;
	dim = density.dim();
	width = new double[dim];
	
	for (int i=0; i<dim; i++)
	    width[i] = density.upperBound[i] - density.lowerBound[i];
    }
    
    public double[] nextPoint(double[] currentPoint) {
	double[] result = new double[dim];

	for (int i=0; i<dim; i++) 
	    result[i] = density.lowerBound[i] + (random.nextDouble() * width[i]);
	
	return result;
    }
}
