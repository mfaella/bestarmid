package mab.finder;

import mab.Result;
import mab.util.Gumbel;

/** Boltzmann-Gumbel exploration with user-provided constant C.
 *
 *  A.k.a. "Boltzmann done right" from Cesa-Bianchi et al., NIPS 2017.
 */
public class BoltzmannDoneRight extends AbstractMaxFinder {

    private final double C, C2;

    public BoltzmannDoneRight(double C) {
        this.C = C;
        this.C2 = C * C;
    }

    @Override
    public Result body() {
        pullAllArms(1);

        while (totalSampleCount < budget) {
            int nextArm = boltzmannGumbel();
            pull(nextArm, 1);
        }
        return theMostPromising();
    }

    private int boltzmannGumbel() {
        int maxArm = -1;
        double max = Double.NEGATIVE_INFINITY;
        for (int i=0; i<K; i++) {
            double bonus = Gumbel.sample() * Math.sqrt(C2/results.get(i).sampleCount);
            double value = results.get(i).value + bonus;
            if (value > max) {
                max = value;
                maxArm = i;
            }
        }
        return maxArm;
    }

    @Override
    public String getTag() { return "BGE"; }
}
