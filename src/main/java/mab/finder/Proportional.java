package mab.finder;

import mab.Globals;
import mab.Result;
import mab.util.DistributionOfMax;
import integrate.Integrator;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.ArrayList;
import java.util.List;

public class Proportional extends AbstractMaxFinder {

    private final int phases;
    private Integrator integratorType;

    public Proportional(int phases) {
        this(phases, Integrator.Trapezoid);
    }

    public Proportional(int phases, Integrator integratorType) {
        if (phases <= 0)
            throw new IllegalArgumentException("Cannot work with 0 phases");

        this.phases = phases;
        this.integratorType = integratorType;
    }

    @Override
    protected Result body() {
        if (budget <= 2 * K)
            throw new IllegalArgumentException("I need at least 2 samples per arm");

        NormalDistribution[] normals = new NormalDistribution[K];
        // initialise with a uniform distribution over [K]
        int[] index = new int[K];
        double[] probabilities = new double[K];
        double uniformProbability = 1.0/K;
        for (int i =0;i<K;i++) {
            probabilities[i] = uniformProbability;
            index[i] = i;
        }
        EnumeratedIntegerDistribution preferenceDistribution;

        int firstPhaseBudget = budget / phases;
        // if possible, start with 2 samples per arm
        if (firstPhaseBudget<2*K && 2*K <= budget) {
            firstPhaseBudget = 2*K;
        }
        pullAllArms(firstPhaseBudget/K);
        budget -= (firstPhaseBudget/K) * K; // integer division
        int phaseBudget = budget / (phases - 1);

        if (Globals.debug())
            System.err.println("First phase budget: " + firstPhaseBudget + "\t general phase budget: " + phaseBudget);

        DistributionOfMax distributionOfMax = new DistributionOfMax(integratorType);

        for(int phase=1; phase<phases; phase++){
            List<Integer> nullVariance = new ArrayList<>();
            double[] meansWithNullVariance = new double[K];
            // Update normals
            for (int i=0; i<K; i++){
                if (results.get(i).iidStandardError == 0) {
                    // Arm with zero variance
                    normals[i] = null;
                    meansWithNullVariance[i] = results.get(i).value;
                    nullVariance.add(i);
                }
                else normals[i] = new NormalDistribution(results.get(i).value, results.get(i).iidStandardError);
            }

            // Update probabilities by computing integrals
            probabilities = distributionOfMax.get(normals, nullVariance, meansWithNullVariance);

            // DEBUG
            if (Globals.debug()) {
                System.err.println("phase: " + phase);
                debugPrintEstimates(normals, meansWithNullVariance, probabilities);
                //double sum = Arrays.stream(probabilities).sum();
                //System.out.println("Sum: " + sum);
                //if (sum < 0.9 || sum > 1.0)
                //     throw new AssertionError("crazy sum:" + sum);
            }
            preferenceDistribution = new EnumeratedIntegerDistribution(index, probabilities);
            pullAccordingToDistribution(preferenceDistribution, phaseBudget);
        }

	    return theMostPromising();
	}

    private static void debugPrintEstimates(NormalDistribution[] estimates, double[] meansWithNoVariance, double[] probs) {
        int i = 0;
        for (NormalDistribution d: estimates) {
            if (d != null)
                System.err.println("mean: " + d.getMean() + "\t stdev: " + d.getStandardDeviation() + "\t prob: " + probs[i]);
            else
                System.err.println("mean: " + meansWithNoVariance[i] + "\t prob: " + probs[i]);
            i++;
        }
    }

    @Override
    public String getTag() { return "Prop"; }
}
