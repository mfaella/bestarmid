package mab.finder;

import mab.Result;
import mab.util.NormalCumulativeProbability;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FastProportional extends AbstractMaxFinder {

    private final int phases;

    public FastProportional(int phases) {
        if (phases <= 0)
            throw new IllegalArgumentException("Cannot work with 0 phases");

        this.phases = phases;
    }

    @Override
    protected Result body() {


        // initialise with an uniform distribution over [K]
        int[] index = new int[K];
        for (int i =0;i<K;i++) {
            index[i] = i;
        }

        int firstPhaseBudget = budget / phases;
        // if possible, start with 2 samples per arm
        if (firstPhaseBudget<2*K && 2*K <= budget) {
            firstPhaseBudget = 2*K;
        }
        pullAllArms(firstPhaseBudget/K);
        budget -= (firstPhaseBudget/K) * K; // integer division
        int phaseBudget = budget / (phases - 1);

        for(int phase=1; phase<phases; phase++){


            double[] probabilities = getProbabilities();

            // DEBUG
            // debugPrintEstimates(normals, probabilities);
            double sum = Arrays.stream(probabilities).sum();
            // System.out.println("Sum: " + sum);
            if (sum < 0.1)
                throw new AssertionError("crazy sum!");

            EnumeratedIntegerDistribution preferenceDistribution = new EnumeratedIntegerDistribution(index, probabilities);
            pullAccordingToDistribution(preferenceDistribution, phaseBudget);
        }
        return theMostPromising();
    }

    private static void debugPrintEstimates(NormalDistribution[] estimates, double[] probs) {
        int i = 0;
        for (NormalDistribution d: estimates)
            System.out.println(d.getMean() + "\t" + d.getStandardDeviation() + "\t" + probs[i++]);
    }


    private double[] getProbabilities() {
        double[] probabilities = new double[K];

        // Find top arm and next-to-top arm
        // in case of multiple arms yielding the same top value alpha, both maxMean and viceMaxMean are set to alpha
        int leader = -1, viceLeader = -1;
        double maxMean = Double.NEGATIVE_INFINITY, viceMaxMean = Double.NEGATIVE_INFINITY;
        for (int index=0; index < results.size(); index++) {
            double mean = results.get(index).getValue();
            if (mean > maxMean) {
                viceLeader = leader;
                viceMaxMean = maxMean;
                maxMean = mean;
                leader = index;
            } else {
                if (mean > viceMaxMean) {
                    viceLeader = index;
                    viceMaxMean = mean;
                }
            }
        }

        for (int index = 0; index < probabilities.length; index++) {
                double mean = results.get(index).getValue(),
                        stdDev = results.get(index).iidStandardError,
                        otherMean = 0, otherStdDev = 0;

                if (index == leader) {
                    otherMean = viceMaxMean;
                    otherStdDev = results.get(viceLeader).iidStandardError;
                } else {
                    otherMean = maxMean;
                    otherStdDev = results.get(leader).iidStandardError;
                }
                probabilities[index] = probabilityOfBeingGreater( mean, stdDev , otherMean, otherStdDev);
        }


        return probabilities;
    }


    private static double probabilityOfBeingGreater(double mean1, double std1, double mean2, double std2){
        //this applies to discrete distributions like bernulli in case of multiple top arms
        //for what concern correctness, it could be placed in the (std1==0 && std2==0) case,
        //however here it improves performances
        //we chose the value 0.5 since is the same value you would get in case of non-zero std
        if(mean1==mean2)
            return 0.5;

        if(std1!=0 && std2!=0)
            return NormalCumulativeProbability.eval(
                    mean2 - mean1,
                    Math.sqrt(std1*std1 + std2*std2), 0);
        else if(std1==0 && std2==0)
             return (mean1>mean2)? 1:0;
        else if(std1!=0 && std2==0)
            return 1- NormalCumulativeProbability.eval(
                    mean1, std1, mean2);
        else
            return NormalCumulativeProbability.eval(
                    mean2, std2, mean1);
    }

    @Override
    public String getTag() { return "FastProp"+phases; }
}
