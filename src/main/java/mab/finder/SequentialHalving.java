package mab.finder;

import mab.*;

import java.util.*;

/** Sequential Halving algorithm from Karnin, Koren, Somekh (ICML 2013) */
public class SequentialHalving extends AbstractMaxFinder
{
    // Inputs
    private int[] sampleCount;
    private int maxRounds;

    private final Comparator<Integer> ComparatorByValue =
            Comparator.comparingDouble(id -> results.get(id).value);

    private void initSampleAllocation(int budget) {
        int Kd=K;
        this.maxRounds= (int) Math.ceil(Math.log(K)/Math.log(2));

        int totalSampleCount = 0;
        this.sampleCount = new int[maxRounds+1];
        for (int phase=1; phase<=maxRounds; phase++) {

            int increment = (int) Math.max(1, Math.floor(budget / (Kd * Math.ceil(Math.log(K)/Math.log(2)))));

            // Correction due to having attributed at least 1 sample in the first phase
            if (phase == maxRounds) {
                increment = (int) Math.floor((budget - totalSampleCount) / Kd);
            }
            sampleCount[phase] = sampleCount[phase-1] + increment;
            totalSampleCount += increment * Kd;

            Kd = (int) Math.ceil((double) Kd/2);
            if (Globals.debug())
                System.out.println("HR sampleCount[" + phase + "] " + sampleCount[phase]);
        }
    }

    @Override
    public Result body() {
        initSampleAllocation(budget);

        Set<Integer> queue = new HashSet<>();
        // Fill the queue
        for (int i=0; i < K; i++) {
            queue.add(i);
        }

        int round = 0; 

        // For each round
        while (round < maxRounds) {

            int budgetPerFunction = sampleCount[round+1] - sampleCount[round];

            if (Globals.debug())
                System.out.println("round " + round + "\t functions " + queue.size() + " \t budgetPerFunction " + budgetPerFunction);

            ArrayList<Integer> rejectList= new ArrayList<>();
            // For each function
            for (Integer candidate: queue) {
                if (budgetPerFunction > 0) {
                    pull(candidate, budgetPerFunction);
                    rejectList.add(candidate);
                }
                //if (Globals.debug())
                //    System.out.println("f" + candidate.id + "\t val " + candidate.result.value + "\t sd " + candidate.result.standardError);
            } // end of round (end inner while)
            round++;
            rejectList.sort(ComparatorByValue);
	    
            int toBeRemoved = queue.size() - (int) Math.ceil(queue.size()/2.0);
	    
            for (int i=0; i<toBeRemoved; i++){
                int c = rejectList.get(i);
                queue.remove(c);
            }
        } // end outer while

        if (queue.size() != 1)
            throw new AssertionError("Internal error");
	    return theMostPromising();
    }

    @Override
    public String getTag() { return "SH"; }
}
