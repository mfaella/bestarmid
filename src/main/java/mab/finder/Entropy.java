package mab.finder;

import integrate.Integrator;
import integrate.Summary;
import mab.Result;
import mab.util.DistributionOfMax;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Entropy extends AbstractMaxFinder {

    private final DistributionOfMax distributionOfMax;

    private static boolean DEBUG = false;

    public Entropy() {
        this(Integrator.Trapezoid);
    }

    public Entropy(Integrator integratorType) {
        this.distributionOfMax = new DistributionOfMax(integratorType);
    }

    @Override
    protected Result body() {

        final int initialBudget = 2;
        if (budget <= initialBudget * K)
            throw new IllegalArgumentException("Cannot work with budget " + budget + "");

        pullAllArms(initialBudget);

        NormalDistribution[] normals = new NormalDistribution[K];
        List<Integer> nullVariance = new ArrayList<>();

        // Initialize true normals and list of zero-variance
        for(int i=0; i<K; i++){
            if (results.get(i).iidStandardError == 0) {
                //arm con varianza nulla
                normals[i] = null;
                nullVariance.add(i);
                throw new AssertionError("Zero variance not supported by this finder");
            }
            else normals[i]= new NormalDistribution(null, results.get(i).value, results.get(i).iidStandardError);
        }

        final int granularity = 10;

        // Warning: nullVariance cannot change with this algorithm!
        double[] entropy = new double[K];
        for (int phase=0; phase < (budget-initialBudget*K); phase += granularity){
            for (int arm=0; arm < K; arm++) {
                if (nullVariance.contains(arm)) {
                    continue;
                }
                NormalDistribution[] postNormals = Arrays.copyOf(normals, normals.length);
                Summary armResult = results.get(arm);
                double newStandardError = (armResult.iidStandardError *
                        Math.sqrt(armResult.sampleCount-1)) / (Math.sqrt(armResult.sampleCount));
                postNormals[arm] = new NormalDistribution(null, armResult.value, newStandardError);
                double[] postDistrib = distributionOfMax.get(postNormals);
                entropy[arm] = entropy(postDistrib);
                if (DEBUG)
                    System.err.println("\t" + arm + ":\t" + entropy[arm]);
            }

            int minArm = pickOneArm(entropy);

            if (DEBUG)
                System.err.println(phase + "\t minArm: " + minArm + "\t minEntropy: " + entropy[minArm]);

            Summary newResult = pull(minArm, granularity);
            normals[minArm] = new NormalDistribution(null, newResult.value, newResult.iidStandardError);
            if (DEBUG)
                debugPrintEstimates(normals);
        }

        return theMostPromising();
    }

    private static final Random rand = new Random();

    private int pickOneArm(double[] entropy) {
        // Entropies that are closer than DELTA to the minimum entropy
        // will all be considered minimal.
        final double DELTA = 1E-14;

        double minEntropy = Double.POSITIVE_INFINITY;
        for (int arm=0; arm < K; arm++) {
            if (entropy[arm] < minEntropy) {
                minEntropy = entropy[arm];
            }
        }
        List<Integer> minimal = new ArrayList<>(K);
        for (int arm=0; arm < K; arm++) {
            if (entropy[arm] <= minEntropy + DELTA) {
                minimal.add(arm);
            }
        }
        if (minimal.size() == 1) {
            return minimal.get(0);
        } else {
            System.err.println("Entropy race was close");
            return minimal.get(rand.nextInt(minimal.size()));
        }
    }

    private void debugPrintEstimates(NormalDistribution[] estimates) {
        double[] probs = distributionOfMax.get(estimates);
        int i = 0;
	    double tot = 0;
        for (NormalDistribution d: estimates) {
            System.err.println(i + ":\t" + d.getMean() + "\t" + d.getStandardDeviation() + "\t" + probs[i]);
	    tot += probs[i];
            i++;
        }
	    System.err.println("Tot: " + tot);
        if (tot < 0.8)
            throw new AssertionError("Total is too low");
    }


    private static void debugPrintEstimates(NormalDistribution[] estimates, double[] probs) {
        int i = 0;
        for (NormalDistribution d: estimates)
            System.err.println(i++ + ":\t" + d.getMean() + "\t" + d.getStandardDeviation() + "\t" + (probs!=null?probs[i]:""));
    }

    private double entropy(double[] distrib) {
    	double result = 0;
	    for (double p: distrib) {
	        if (p!=0) {
		        result += - p * Math.log(p);
	        }
	    }
        //System.err.println(Arrays.toString(distrib) + ": " + result);
	    return result;
    }

    @Override
    public String getTag() { return "H"; }
}
