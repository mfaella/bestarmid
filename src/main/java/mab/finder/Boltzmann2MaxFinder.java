package mab.finder;

import mab.Result;
import mab.util.Gumbel;

/** Boltzmann exploration with user-provided constant temperature.
 *
 *  A.k.a. "Boltzmann done wrong" according to Cesa-Bianchi et al., NIPS 2017.
 */
public class Boltzmann2MaxFinder extends AbstractMaxFinder {

    private final double temperature;

    public Boltzmann2MaxFinder(double temperature) {
        this.temperature = temperature;
    }

    @Override
    protected Result body() {
        pullAllArms(1);
        while (totalSampleCount < budget) {
            int nextArm = boltzmannArm();
            pull(nextArm, 1);
        }
        return theMostPromising();
    }

    private int boltzmannArm() {
        int maxArm = -1;
        double max = Double.NEGATIVE_INFINITY;
        for (int i=0; i<K; i++) {
            double bonus = Gumbel.sample();
            double value = results.get(i).value / temperature + bonus;
            if (value > max) {
                max = value;
                maxArm = i;
            }
        }
        return maxArm;
    }

    @Override
    public String getTag() { return "Boltz"; }
}
