package mab.finder;

import mab.*;
import integrate.Summary;
import mab.util.Moments;

import java.util.*;

/**
 * VBR without phases
 */
public class VBR extends AbstractMaxFinder implements CheatingFinder
{
	// Inputs
	private final double delta;
	private final UncertaintyType utype;
	private SelectionCriterion sel;
	private List<Moments> moments;

	// Internal
	private static final boolean saturateBudget = true;
	private static final boolean discardAtLeastOne = false;

	private final int initialSamplesPerArm;
	private static final int ABSENT = -1;

	@Override
	public void setGroundTruth(List<Moments> groundTruth) {
		this.moments = groundTruth;
	}

	private static final Comparator<Candidate> comparatorById =
			Comparator.comparingInt(cand -> cand.id);

	private Comparator<Candidate> comparatorByValue() {
		return Comparator.comparingDouble(cand -> results.get(cand.id).value);
	}

	private static Comparator<Candidate> comparatorByLowerBound(double gamma) {
		return Comparator.comparingDouble(cand -> cand.getLowerBound(gamma));
	}

    	private static Comparator<Candidate> comparatorByUpperBound(double gamma) {
		return Comparator.comparingDouble(cand -> cand.getUpperBound(gamma));
	}

	private static final Comparator<Candidate> comparatorByUncertainty =
			/** There are two uncertainty estimators inside [result]:
			 iidStandardError
			 standardError

			 Check which one is more appropriate.
			 For VBR, these comments were left in the code:
			 - For standardError:
			 Current general-purpose implementation, as per ECAI 2020.
			 - For iidStandardError:
			 It assumes that the samplers return iid samples, so that error estimation is simpler.
			 Use on: samplers based on simple distributions that can be sampled directly (Gaussian).
			 Do not use on: samplers based on MCMC.
			 **/
			Comparator.comparingDouble(cand -> cand.uncertainty);

	public enum UncertaintyType {
		LALITHA("L"), TRUE_VARIANCE("T"), EMP_VARIANCE("E");
		UncertaintyType(String tag) { this.tag = tag; }
		public final String tag;
	};

	public enum SelectionCriterion {
	    ROUND_ROBIN("R"), MAX_UNCERTAINTY("U"), TOP_BOTTOM("TB");
	    SelectionCriterion(String tag) { this.tag = tag; }
	    public final String tag;
	};

	private class Candidate {
		int id;
		double uncertainty;

		double getLowerBound(double gamma) {
			if (results.get(id).sampleCount == 0)
				return Double.NEGATIVE_INFINITY;
			else
				return results.get(id).value - gamma * uncertainty;
		}
		double getUpperBound(double gamma) {
			if (results.get(id).sampleCount == 0)
				return Double.POSITIVE_INFINITY;
			else
				return results.get(id).value + gamma * uncertainty;
		}
		void updateUncertainty() {
			Summary result = results.get(id);
			switch (utype) {
				case TRUE_VARIANCE:
					double trueVariance = moments.get(id).sd * moments.get(id).sd;
					uncertainty = Math.sqrt(trueVariance / result.sampleCount);
					break;
				case EMP_VARIANCE:
					uncertainty = result.iidStandardError;
					break;
				case LALITHA:
					double empiricalVariance =
							result.iidStandardError * result.iidStandardError * result.sampleCount;
					// This is U/N in the paper by Lalitha et al.
					uncertainty =
							empiricalVariance /
									(result.sampleCount * (1 - 2*Math.sqrt(Math.log(1.0/delta)/(result.sampleCount-1))));
					uncertainty = Math.sqrt(uncertainty);
					break;
			}
		}

	    @Override
	    public String toString() {
		return id + ", " + results.get(id).value + ", unc: " + uncertainty;
	    }
	}

	public VBR(double delta, UncertaintyType utype, SelectionCriterion sel) {
		if (delta < 0)
			throw new IllegalArgumentException("delta must be non-negative.");
		this.delta = delta;
		this.initialSamplesPerArm = ABSENT;
		this.utype = utype;
		this.sel = sel;
	}

	public VBR(int initialSamplesPerArm, UncertaintyType utype, SelectionCriterion sel) {
		if (initialSamplesPerArm < 0)
			throw new IllegalArgumentException("initialSamplesPerArm must be non-negative.");
		if (utype == UncertaintyType.LALITHA)
			throw new IllegalArgumentException("This constructor is incompatible with this uncertainty type.");
		this.initialSamplesPerArm = initialSamplesPerArm;
		this.delta = ABSENT;
		this.utype = utype;
		this.sel = sel;
	}

	@Override
	protected Result body() {
		List<Candidate> liveArms = new ArrayList<>();
		Set<Candidate> allArms = new HashSet<>();

		// The phase-based sample allocation from SR (for comparison purposes)
		int[] sampleCount = new int[K];
		double log = 0.5;
		for (int i = 2; i <= K; i++)
			log += 1 / (double) i;
		// The n_k vector from SR
		for (int phase = 1; phase < K; phase++) {
			sampleCount[phase] = (int) Math.ceil((budget - K) / ((K + 1 - phase) * log));
		}

		// Fill the queue
		for (int i=0; i<K; i++) {
			Candidate c = new Candidate();
			c.id = i;
			c.uncertainty = Double.POSITIVE_INFINITY;
			liveArms.add(c);
			allArms.add(c);
		}

		int minimumSamples = initialSamplesPerArm;
		if (minimumSamples == ABSENT) {
			minimumSamples = initialSamplesPerArmFromUncertaintyType(sampleCount[1]);
		}

		if (budget < K * minimumSamples)
			throw new RuntimeException("This algorithm needs at least " + minimumSamples + " samples per arm to work.");
		// System.err.println(getTag() + " uniform samples: " + minimumSamples);
		pullAllArms(minimumSamples);
		
		for (Candidate c: liveArms) {
		    c.updateUncertainty();
		}		
		
		if (Globals.debug())
			 System.out.println("Uniformly:" + (K * minimumSamples));

		int t = K * minimumSamples;
		double sdMultiplier = 2; // aka gamma
		Candidate cutThresholdOwner = Collections.max(liveArms, comparatorByLowerBound(sdMultiplier));
		double cutThreshold = cutThresholdOwner.getLowerBound(sdMultiplier);
		int roundRobinIndex = 0;
		int phase = 1;
		
		while (liveArms.size() > 1 && t < budget) {

			if (Globals.debug())
				System.out.println("VBR t=" + t + "\t functions " + liveArms.size() + "\t threshold owner: " + cutThresholdOwner);

			// Selection
			Candidate current = null;
			do { // Loop until we select a valid candidate
			    switch (sel) {
			    case TOP_BOTTOM:
				Iterator<Candidate> iter = liveArms.iterator();
				while (iter.hasNext()) {
				    Candidate c = iter.next();
				    if (c.getUpperBound(sdMultiplier) < cutThreshold)
					iter.remove();
				}
				// We only pull either the best or the worst arm
				if (cutThresholdOwner == null) {
				    current = liveArms.get(0);
				} else {
				    Candidate top = Collections.max(liveArms, comparatorByValue());
				    Candidate bottom = Collections.min(liveArms, comparatorByValue());
				    current = top.uncertainty > bottom.uncertainty ? top : bottom;
				}
				break;
			    case ROUND_ROBIN:
				current = liveArms.get(0);
				break;
			    case MAX_UNCERTAINTY:
				current = Collections.max(liveArms, comparatorByUncertainty);
				break;
			    }
			    liveArms.remove(current);
			} while (!liveArms.isEmpty() && current.getUpperBound(sdMultiplier) < cutThreshold);

			if (Globals.debug())
				System.out.println("VBR t=" + t + "\t current: " + current);
			
			if (!liveArms.isEmpty()) {
				// Pull
				pull(current.id, 1);
				t++;
				current.updateUncertainty();
				Summary result = results.get(current.id);

				liveArms.add(current);

				if (Globals.debug())
					System.out.println("f" + current.id + "\t val " + result.value
							+ "\t sd " + result.standardError
							+ "\t simpleSd " + result.iidStandardError);

				// System.err.println("Arm:" + current.id + "\tempVar:" + empiricalVariance + "\t" + current.result.sampleCount + "\tQueue:" + queue.size());
				// System.err.println("\t" + current.uncertainty);

				double lowerBound = current.getLowerBound(sdMultiplier);
				// double upperBound = current.getUpperBound(sdMultiplier);

				// Raise the cut threshold
				if (lowerBound > cutThreshold) {
					cutThresholdOwner = current;
					cutThreshold = lowerBound;
				}

				// Lower the cut threshold
				if (cutThresholdOwner == current && lowerBound < cutThreshold) {
					// Find the new maximum lower bound
					cutThresholdOwner = Collections.max(liveArms, comparatorByLowerBound(sdMultiplier));
					cutThreshold = cutThresholdOwner.getLowerBound(sdMultiplier);
				}

				// Remove at least one
				if (discardAtLeastOne && t == sampleCount[phase]) {
					Candidate leastPromising = Collections.min(liveArms, comparatorByValue());
					liveArms.remove(leastPromising);
					phase++;
				}
				// printQueue(liveArms);
				// System.err.printf("Threshold: %.3f (%d)\n", cutThreshold, cutThresholdOwner.id);
			}

			// Catch-up step: we still have budget, but at most 1 candidate
			if (saturateBudget && liveArms.size() <= 1 && t < budget) {
				sdMultiplier *= 1.5;
				liveArms.addAll(allArms);
				cutThresholdOwner = Collections.max(liveArms, comparatorByLowerBound(sdMultiplier));
				cutThreshold = cutThresholdOwner.getLowerBound(sdMultiplier);
				// System.err.print(" " + sdMultiplier + " ");
			}
		}
		// System.err.println("\tNon usati:" + (residualBudget - t) + "\t in coda:" + liveArms.size());
		// System.err.println(experimentCounter + " VBR2 alloc: " + Arrays.toString(allocation));
		return theMostPromising();
	}

	private int initialSamplesPerArmFromUncertaintyType(int sampleCountFromSR) {
		switch (utype) {
			case LALITHA:
				return (int) Math.ceil(4 * Math.log(1/delta) + 1);
			case EMP_VARIANCE:
				return sampleCountFromSR;
			case TRUE_VARIANCE:
				return 1;
			default:
				return 0;
		}
	}

	@Override
	public String getTag() {
		if (initialSamplesPerArm == ABSENT)
			return "VBR" + utype.tag + sel.tag;
		else
			return "VBR" + utype.tag + initialSamplesPerArm;
	}

	private void printQueue(Collection<Candidate> queue) {
		for (Candidate c: queue) {
			Summary result = results.get(c.id);
			System.err.printf("(%d, %.3f, %.3f, %d) ", c.id, result.value, c.uncertainty, result.sampleCount);
		}
		System.err.println("");
	}
}
