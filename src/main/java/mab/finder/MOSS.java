package mab.finder;

import integrate.Summary;
import mab.Result;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class MOSS extends AbstractMaxFinder {

    private static class Candidate {
        int id;
        double ucb;
    }

    @Override
    public Result body() {
        SortedSet<Candidate> arms = new TreeSet<>(
                Comparator.<Candidate>comparingDouble(c -> c.ucb)
                          .thenComparingInt(c -> c.id)
        );
        final double avgBudget = budget / (double) K;
        pullAllArms(1);
        for (int i=0; i < K; i++) {
            Candidate c = new Candidate();
            c.id = i;
            c.ucb = results.get(i).value + Math.sqrt(Math.max(0, Math.log(avgBudget/1))/1);
            arms.add(c);
        }
        while (totalSampleCount < budget) {
            Candidate best = arms.last();
            int arm = best.id;
            arms.remove(best);
            Summary summary = pull(arm, 1);
            best.ucb = summary.value + Math.sqrt(Math.max(0, Math.log(avgBudget/summary.sampleCount))/summary.sampleCount);
            arms.add(best);
        }
        return theMostPromising();
    }

    @Override
    public String getTag() { return "MOSS"; }
}
