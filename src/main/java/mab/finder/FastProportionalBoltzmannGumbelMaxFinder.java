package mab.finder;

import mab.Result;
import mab.util.Gumbel;
import mab.util.NormalCumulativeProbability;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FastProportionalBoltzmannGumbelMaxFinder extends AbstractMaxFinder {

    private final int phases;

    public FastProportionalBoltzmannGumbelMaxFinder(int phases) {
        if (phases <= 0)
            throw new IllegalArgumentException("Cannot work with 0 phases");

        this.phases = phases;
    }

    @Override
    protected Result body() {

        List<Integer> nullVariance;

        // initialise with an uniform distribution over [K]
        int[] index = new int[K];
        for (int i =0;i<K;i++) {
            index[i] = i;
        }

        int firstPhaseBudget = budget / phases;
        // if possible, start with 2 samples per arm
        if (firstPhaseBudget<2*K && 2*K <= budget) {
            firstPhaseBudget = 2*K;
        }
        pullAllArms(firstPhaseBudget/K);
        budget -= (firstPhaseBudget/K) * K; // integer division
        int phaseBudget = budget / (phases - 1);

        for(int phase=1; phase<phases; phase++){
            nullVariance = new ArrayList<>();

            //update normals
            for(int i=0; i<K; i++){
                if (results.get(i).iidStandardError == 0) {
                    //arm con varianza nulla
                    nullVariance.add(i);
                    //throw new AssertionError("Zero variance in samples");
                }
                // else normals[i]= new NormalDistribution(results.get(i).value, results.get(i).iidStandardError);
            }

            double[] probabilities = getProbabilities(nullVariance);

            // DEBUG
            // debugPrintEstimates(normals, probabilities);
            double sum = Arrays.stream(probabilities).sum();
            // System.out.println("Sum: " + sum);
            if (sum < 0.1)
                throw new AssertionError("crazy sum!");
            for(int j=0;j<phaseBudget;j++){
                int nextArm=boltzmannArm(probabilities);
                pull(nextArm, 1);
            }

//            EnumeratedIntegerDistribution preferenceDistribution = new EnumeratedIntegerDistribution(index, probabilities);
//            pullAccordingToDistribution(preferenceDistribution, phaseBudget);
        }
        return theMostPromising();
    }


    private int boltzmannArm(double[] probabilities) {
        int maxArm = -1;
        double max = Double.NEGATIVE_INFINITY;
        for (int i=0; i<K; i++) {
            double bonus = Gumbel.sample();
            double value = probabilities[i]/0.05  + bonus;
            if (value > max) {
                max = value;
                maxArm = i;
            }
        }
        return maxArm;
    }


    private static void debugPrintEstimates(NormalDistribution[] estimates, double[] probs) {
        int i = 0;
        for (NormalDistribution d: estimates)
            System.out.println(d.getMean() + "\t" + d.getStandardDeviation() + "\t" + probs[i++]);
    }


    private double[] getProbabilities(List<Integer> nullVariance) {
        double[] probabilities = new double[K];

        // Find top arm and next-to-top arm
        int leader = -1, viceLeader = -1;
        double maxMean = Double.NEGATIVE_INFINITY, viceMaxMean = Double.NEGATIVE_INFINITY;
        for (int index=0; index < results.size(); index++) {
            double mean = results.get(index).getValue();
            if (mean > maxMean) {
                viceLeader = leader;
                viceMaxMean = maxMean;
                maxMean = mean;
                leader = index;
            } else {
                if (mean > viceMaxMean) {
                    viceLeader = index;
                    viceMaxMean = mean;
                }
            }
        }

        if(nullVariance.isEmpty()) {
            for (int index = 0; index < probabilities.length; index++) {
                double mean = results.get(index).getValue(),
                        stdDev = results.get(index).iidStandardError,
                        otherMean = 0, otherStdDev = 0;

                if (index == leader) {
                    otherMean = viceMaxMean;
                    otherStdDev = results.get(viceLeader).iidStandardError;
                } else {
                    otherMean = maxMean;
                    otherStdDev = results.get(leader).iidStandardError;
                }
                probabilities[index] = NormalCumulativeProbability.eval(
                        otherMean - mean,
                        Math.sqrt(stdDev*stdDev + otherStdDev*otherStdDev), 0);
            }
        } else {
            throw new RuntimeException("Unsupported");
        }

        return probabilities;
    }

    @Override
    public String getTag() { return "FastPropBolt"; }
}
