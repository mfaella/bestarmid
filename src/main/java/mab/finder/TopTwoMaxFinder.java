package mab.finder;

import integrate.Summary;
import mab.Result;

import java.util.*;

/** This finder allocates each pull to the arm with the greatest standard error
    among the current top two arms.
*/  
public class TopTwoMaxFinder extends AbstractMaxFinder {

    private static class Candidate implements Comparable<Candidate> {
        int index;
        Summary result;

        @Override
        public int compareTo(Candidate other) {
            return Double.compare(result.value, other.result.value);
        }
    }


    @Override
    protected Result body() {
        SortedSet<Candidate> candidates = new TreeSet<>();

        // Initialization phase: 2 samples per arm
        pullAllArms(2);

        for (int arm = 0; arm < K; arm++){
            Candidate c = new Candidate();
            c.index = arm;
            c.result = results.get(arm);
            candidates.add(c);
        }

        // debugPrint(candidates);

        for (int i=totalSampleCount; i<budget; i++) {
            Candidate first = candidates.last();
            Candidate second = candidates.headSet(first).last();
            Candidate chosen = first.result.iidStandardError > second.result.iidStandardError? first : second;

            pull(chosen.index, 1);
            // Update mean and variance
            candidates.remove(chosen);
            chosen.result = results.get(chosen.index);
            candidates.add(chosen);
        }
        return theMostPromising();
    }

    private void debugPrint(SortedSet<Candidate> set) {
        for (Candidate c: set) {
            System.out.println(c.result.value + " (" + c.result.iidStandardError + ")");
        }
    }

    @Override
    public String getTag() { return "Top2"; }
}
