package mab.finder;

import java.util.List;
import mab.util.Moments;

public interface CheatingFinder {
    void setGroundTruth(List<Moments> groundTruth);
}
