package mab.finder;

import mab.Result;
import org.apache.commons.math3.distribution.*;
import org.apache.commons.math3.random.Well19937c;

import java.util.Random;

public class RandomizedStudThomSampling  extends AbstractMaxFinder{
    private static final Well19937c randomGenerator=new Well19937c();
    private static final double samplingAccuracy = 1E-3;


    @Override
    protected Result body() {
        Random coin=new Random();

        if (budget<2*K) throw new RuntimeException("Insufficient budget.");

        pullAllArms(2);
        int runningBudget = budget - 2*K;

        RealDistribution[] belief = new RealDistribution[K];
        for (int arm=0; arm<K; arm++) {
            belief[arm] = new TDistribution(randomGenerator, 1, samplingAccuracy);
        }

        for(;runningBudget>0; runningBudget--){
            //System.err.println(runningBudget);
            int leader=-1;
            int second=-1;
            int chosen;
            double maxMean = Double.NEGATIVE_INFINITY;
            double secondMean = Double.NEGATIVE_INFINITY;
            for(int arm=0;arm<K;arm++){
                double sample = results.get(arm).getValue() +
                        results.get(arm).iidStandardError * belief[arm].sample();
                if (sample>maxMean) {
                    secondMean=maxMean;
                    second=leader;
                    maxMean = sample;
                    leader = arm;
                }
                else if(sample>secondMean){
                    secondMean = sample;
                    second = arm;
                }
            }
            chosen= coin.nextInt(100)<50?  leader : second;


            pull(chosen, 1);
            belief[chosen] = new TDistribution(randomGenerator, results.get(chosen).sampleCount-1, samplingAccuracy);
        }
        return theMostPromising();
    }


    @Override
    public String getTag() { return "RTTS"; }



}
