package mab.finder;

import mab.Result;
import mab.util.NormalCumulativeProbability;
import org.apache.commons.math3.random.Well19937c;


public class FastUTS extends AbstractMaxFinder{
    private static final Well19937c randomGenerator=new Well19937c();

    @Override
    protected Result body() {
        if (budget<2*K) throw new RuntimeException("Insufficient budget.");

        pullAllArms(2);
        int runningBudget = budget - 2*K;

        for(;runningBudget>0; runningBudget--){
            int leader=-1;
            double maxMean = Double.NEGATIVE_INFINITY;
            for(int arm=0;arm<K;arm++){
                double sample;
                if (results.get(arm).iidStandardError>0) {
                    sample = NormalCumulativeProbability.gaussianSample(
                            results.get(arm).value, results.get(arm).iidStandardError);
                } else {
                    sample = results.get(arm).value;
                }
                if (sample>maxMean) {
                    maxMean = sample;
                    leader = arm;
                }
            }
            pull(leader, 1);
        }
        return theMostPromising();
    }

    @Override
    public String getTag() { return "FUTS"; }
}
