package mab.finder;

import integrate.Summary;
import mab.MaxFinder;
import integrate.Function;
import mab.Result;
import mab.sampler.Sampler;
import integrate.Integral;
import org.apache.commons.math3.distribution.IntegerDistribution;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMaxFinder implements MaxFinder {
    
    protected List<Sampler> samplers;
    protected int budget;

    protected int totalSampleCount;
    protected double totalReward;
    protected int[] allocation;
    protected int K;

    protected int experimentCounter;
	
    // Outputs
    protected int maxIndex;
    protected Summary maxValue;

    protected List<Summary> results;

    protected void init(List<Sampler> arms, int budget) {
	experimentCounter++;
        this.K = arms.size();
        if (budget < K)
            throw new IllegalArgumentException("Budget is too small: I need at least one sample per arm.");

        this.samplers = arms;
        this.results = new ArrayList<>(K);
        this.allocation = new int[K];
        this.budget = budget;
	this.totalSampleCount = 0;
	this.totalReward = 0;

	this.maxValue = null;
	this.maxIndex = -1;
	
        for(int i=0; i<K; i++){
            results.add(null);
        }
    }

    @Override
    public Result compute(List<Sampler> arms, int budget) {
        init(arms, budget);
        return body();
    }

    protected abstract Result body();

    /** Pulls <code>n</code> times arm <code>arm</code>.
     Concrete subclasses should call this method whenever they sample from an arm,
     as it automatically updates all statistics. */
    protected Summary pull(int arm, int n) {
        Sampler sampler = samplers.get(arm);
        List<double[]> samples = sampler.sample(n);
        totalSampleCount += n;
        allocation[arm] += n;
        totalReward += samples.stream().mapToDouble(r -> r[0]).sum();
        // update mean and variance
        Summary oldResult = results.get(arm);
        Summary result;
        if (oldResult == null)
            result = Integral.refine(null, identity, samples);
        else
            result = Integral.refine(oldResult, identity, samples);
        results.set(arm, result);
        return result;
    }

    protected void pullAllArms(int budgetPerArm) {
        for (int arm = 0; arm < K; arm++) {
            pull(arm, budgetPerArm);
        }
    }

    protected void pullAccordingToDistribution(IntegerDistribution armDistribution, int totalBudget) {
        for (int j = 0; j< totalBudget; j++){
            // draw an arm according to the preference
            int arm = armDistribution.sample();
            pull(arm, 1);
        }
    }

    protected Result theMostPromising() {
        // Extract the output
        double max = Double.NEGATIVE_INFINITY;
        for (int i=0; i<results.size(); i++){
            Summary summary = results.get(i);
            if(summary.getValue() > max) {
                max = summary.getValue();
                maxIndex = i;
                maxValue = summary;
            }
        }
        return new Result(maxIndex, maxValue.value, totalSampleCount, totalReward, allocation, results);
    }

    protected Result thisArm(int arm) {
        return new Result(arm, results.get(arm).value, totalSampleCount, totalReward, allocation, results);
    }

    private static final Function identity = Function.make(a -> a[0], 1);
}
