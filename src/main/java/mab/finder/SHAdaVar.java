package mab.finder;

import integrate.Summary;
import mab.Globals;
import mab.Result;

import java.util.*;

/** SHAdaVar algorithm from Lalitha et al. (UAI 2023) */
public class SHAdaVar extends AbstractMaxFinder
{
    private double delta;

    public SHAdaVar(double delta) {
        this.delta = delta;
    }

    private final Comparator<Candidate> ComparatorByValue =
            Comparator.comparingDouble(cand -> results.get(cand.id).value);

    private final Comparator<Candidate> ComparatorByUncertainty =
            Comparator.comparingDouble(cand -> cand.uncertainty);

    private class Candidate {
        int id;
        double uncertainty;

        @Override
        public String toString() {
            if (results.get(id) != null)
                return id + "\tval: " + results.get(id).value + "\tunc: " + uncertainty;
            else
                return id + "\tval: " + "none" + "\tunc: " + uncertainty;
        }
    }

    @Override
    public Result body() {
        int nStages = (int) Math.ceil(Math.log(K)/Math.log(2));

        List<Candidate> candidates = new ArrayList<>();
        // Fill the candidates
        for (int i=0; i < K; i++) {
            Candidate c = new Candidate();
            c.id = i;
            c.uncertainty = Double.POSITIVE_INFINITY;
            candidates.add(c);
        }

        int minimumSamples = K * (int) Math.ceil(4 * Math.log(1/delta) + 1);
        int roundRobinIndex = 0;
        int stage = 0;

        if (Globals.debug())
            System.out.println("nStages: " + nStages + "\t minimumSamples: " + minimumSamples);


        // For each stage
        while (stage < nStages) {

            int stageBudget = (int) Math.floor(budget / nStages);

            if (Globals.debug()) {
                System.out.println("stage: " + stage + "\t functions: " + candidates.size() + "\t stage budget: " +  stageBudget);
                for (Candidate c: candidates)
                    System.out.println(c);
            }

            if (roundRobinIndex >= candidates.size())
                roundRobinIndex = 0;

            for (int i = 0; i<stageBudget; i++) {
                Candidate current = null;
                if (totalSampleCount <= minimumSamples) {
                    current = candidates.get(roundRobinIndex);
                    roundRobinIndex = (roundRobinIndex + 1) % candidates.size();
                } else {
                    current = Collections.max(candidates, ComparatorByUncertainty);
                }
                pull(current.id, 1);
                // Update uncertainty
                Summary result = results.get(current.id);
                double empiricalVariance =
                        result.iidStandardError * result.iidStandardError * result.sampleCount;
                // This is U/N in the paper by Lalitha et al.
                current.uncertainty =
                        empiricalVariance /
                                (result.sampleCount * (1 - 2*Math.sqrt(Math.log(1.0/delta)/(result.sampleCount-1))));
            }

            // End of stage: discard the bottom half
            stage++;
            candidates.sort(ComparatorByValue);
            int toBeRemoved = (int) Math.floor(candidates.size()/2.0);
            for (int i=0; i<toBeRemoved; i++){
                Candidate removed = candidates.remove(0);
                if (Globals.debug())
                    System.out.println("Removing " + removed);
            }
        } // end outer while

        if (candidates.size() != 1)
            throw new AssertionError("Internal error");
	    return theMostPromising();
    }

    @Override
    public String getTag() { return "SHAV"; }
}
