package mab.finder;

import mab.Result;

import java.util.*;

public class Uniform extends FixedAllocationMaxFinder
{
	@Override
	protected Result body() {
		setAllocation(makeAllocation(K, budget));
		return super.body();
	}

	private static int[] makeAllocation(int size, int budget) {
		int[] allocation = new int[size];
		Arrays.fill(allocation, budget/size);//(int) Math.floor(budget/size));
		return allocation;
	}

    @Override
    public String getTag() { return "Unif"; }
}
