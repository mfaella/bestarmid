package mab.finder;

import integrate.Summary;
import mab.Result;
import mab.sampler.Sampler;
import mab.util.Moments;

import java.util.*;

/** This finder knows the ground truth and allocates pulls uniformly,
    but arm i does not receive any more pulls as soon as one of the
    following conditions holds:
       1) i is     the top arm and estimate_i >= true_value_i
       2) i is NOT the top arm and estimate_i <= true_value_i
*/
public class CheatingMaxFinder extends AbstractMaxFinder implements CheatingFinder
{
	private static final boolean DEBUG = false;

	private List<Moments> groundTruth;

    @Override
    public void setGroundTruth(List<Moments> groundTruth) {
	this.groundTruth = groundTruth;
    }

	@Override
	protected Result body() {
	    if (groundTruth==null)
		throw new IllegalStateException("Must call setGroundTruth before.");
	    int trueMaxIndex = 0, i = 0;
	    double trueMaxMean = Double.NEGATIVE_INFINITY;
	    for (Moments m: groundTruth) {
		if (m.mean > trueMaxMean) {
		    trueMaxMean = m.mean;
		    trueMaxIndex = i;
		}
		i++;
	    }
	    Set<Integer> candidates = new HashSet<>();
	    for (int j = 0; j < K; j++) {
		candidates.add(j);
	    }
	    
	    while (budget>0 && !candidates.isEmpty()) {
		Iterator<Integer> it = candidates.iterator();
		while (it.hasNext()) {
				int j = it.next();
				Summary newResult = pull(j, 1);
				budget--;
				if ( (j == trueMaxIndex && newResult.value >= trueMaxMean) ||
				     (j != trueMaxIndex && newResult.value <= groundTruth.get(j).mean))
					it.remove();
		}
	    }
	    return theMostPromising();
	}

    @Override
    public String getTag() { return "Cheat"; }
}
