package mab.finder;

import mab.Result;
import org.apache.commons.math3.distribution.*;
import org.apache.commons.math3.random.Well19937c;


public class StudentThompsonSampling extends AbstractMaxFinder {
    private static final Well19937c randomGenerator=new Well19937c();
    private static final double samplingAccuracy = 1E-3;

    private static final boolean DEBUG = false;

    @Override
    protected Result body() {
        if (budget<2*K) throw new RuntimeException("Insufficient budget.");

        pullAllArms(2);
        int runningBudget = budget - 2*K;

        RealDistribution[] belief = new RealDistribution[K];
        for (int arm=0; arm<K; arm++) {
	        belief[arm] = new TDistribution(randomGenerator, 1, samplingAccuracy);
        }

        for(;runningBudget>0; runningBudget--){
            //System.err.println(runningBudget);
            int leader=-1;
            double maxMean = Double.NEGATIVE_INFINITY;
            for(int arm=0;arm<K;arm++){
                if (DEBUG)
                    System.err.print(arm + ": (" + String.format("%,.3f", results.get(arm).value) + ";" + results.get(arm).sampleCount + ")\t");
                double sample = results.get(arm).value +
		                        results.get(arm).iidStandardError * belief[arm].sample();
                if (sample>maxMean) {
                    maxMean = sample;
                    leader = arm;
                }
            }
            if (DEBUG)
                System.err.println();
            pull(leader, 1);
	        belief[leader] = new TDistribution(randomGenerator, results.get(leader).sampleCount-1, samplingAccuracy);
        }
        return theMostPromising();
    }


    @Override
    public String getTag() { return "TTS"; }
}
