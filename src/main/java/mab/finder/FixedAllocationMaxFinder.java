package mab.finder;

import mab.Result;

public class FixedAllocationMaxFinder extends AbstractMaxFinder
{
    private int[] fixedAllocation;

    protected FixedAllocationMaxFinder setAllocation(int[] allocation) {
        this.fixedAllocation = allocation;
        return this;
    }

    public FixedAllocationMaxFinder() {
    }
    
    public FixedAllocationMaxFinder(int[] allocation) {
	setAllocation(allocation);
    }
    
    @Override
    protected Result body() {
	    for (int i=0; i<K; i++) {
	        pull(i, fixedAllocation[i]);
	    }
        return theMostPromising();
	}

    @Override
    public String getTag() { return "Fixed"; }
}
