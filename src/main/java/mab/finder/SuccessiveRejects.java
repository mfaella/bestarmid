package mab.finder;

import mab.*;

import java.util.*;

/**
 * The SR algorithm from:
 * 	  Audibert et al.
 * 	  Best Arm Identification in Multi-Armed Bandits
 * 	  COLT 2010
 */
public class SuccessiveRejects extends AbstractMaxFinder
{
    private int[] sampleCount;

	@Override
	public Result body() {
		this.sampleCount = new int[K];
		double log = 0.5;
		for (int i=2; i<=K; i++)
			log += 1/(double)i;
		for (int phase=1; phase<K; phase++) {
			sampleCount[phase] = (int) Math.ceil((budget-K) / ((K + 1 -phase) * log));
			if (Globals.debug())
				System.out.println("SR sampleCount[" + phase + "] " + sampleCount[phase]);
			if (sampleCount[phase] == sampleCount[phase-1])
				throw new IllegalArgumentException("Cannot work with budget " + budget);
		}

		Set<Integer> queue = new HashSet<>();
		// Fill the queue
		for (int i=0; i<K; i++) {
			queue.add(i);
		}

		int round = 0, maxRounds = K - 1;

		// For each round
		while (round < maxRounds) {

			int budgetPerFunction = sampleCount[round+1] - sampleCount[round];

			if (Globals.debug())
				System.out.println("round " + round + "\t functions " + queue.size() + " \t budgetPerFunction " + budgetPerFunction);

			double min = Double.POSITIVE_INFINITY;
			int minCandidate = -1;

			// For each function
			for (int candidate: queue) {
				pull(candidate, budgetPerFunction);

				// Update min (eventually discarded)
				if (results.get(candidate).value < min) {
					min = results.get(candidate).value;
					minCandidate = candidate;
				}
			} // end of round (end inner while)
			round++;
			queue.remove(minCandidate);
		} // end outer while

		if (queue.size() != 1)
			throw new AssertionError("Internal error");
		return thisArm(queue.iterator().next());
	}

    @Override
    public String getTag() { return "SR"; }
}
