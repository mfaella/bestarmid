package mab.finder;

import mab.*;
import integrate.Summary;
import mab.sampler.Sampler;
import mab.util.Moments;

import java.util.*;
import java.util.function.ToDoubleFunction;

/**
 * VBR + the exploration boost by Lalitha et al. (UAI23)
 */
public class VBRAdaVar extends AbstractMaxFinder implements CheatingFinder
{
	// Inputs
	private double delta;
	private double sdMultiplier = 2; // aka gamma


	private UncertaintyType utype;
	private SelectionType seltype;

	// Internal
	private ToDoubleFunction<Summary> errorExtractor;
	private int totalBudget;
	private final boolean saturateBudget = true;
	private final boolean discardAtLeastOne = true;

	private List<Moments> moments;

	@Override
	public void setGroundTruth(List<Moments> groundTruth) {
		this.moments = groundTruth;
	}

	private static final Comparator<Candidate> comparatorById =
			Comparator.comparingInt(cand -> cand.id);

	//   private static final Comparator<Candidate> comparatorByValue =
//	Comparator.comparingDouble(cand -> results.get(cand.id).value);

	private static Comparator<Candidate> comparatorByLowerBound(double gamma) {
		return Comparator.comparingDouble(cand -> cand.getLowerBound(gamma));
	}

	private static final Comparator<Candidate> comparatorByUncertainty =
			/** There are two uncertainty estimators inside [result]:
			 iidStandardError
			 standardError

			 Check which one is more appropriate.
			 For VBR, these comments were left in the code:
			 - For standardError:
			 Current general-purpose implementation, as per ECAI 2020.
			 - For iidStandardError:
			 It assumes that the samplers return iid samples, so that error estimation is simpler.
			 Use on: samplers based on simple distributions that can be sampled directly (Gaussian).
			 Do not use on: samplers based on MCMC.
			 **/
			Comparator.comparingDouble(cand -> cand.uncertainty);

	private  class Candidate {
		//Summary result;
		Sampler sampler;
		int id;
		double uncertainty;
		double getLowerBound(double gamma) {
			Summary result=results.get(this.id);
			if (result.sampleCount == 0)
				return Double.NEGATIVE_INFINITY;
			else
				return result.value - gamma * result.iidStandardError;
		}
		double getUpperBound(double gamma) {
			Summary result=results.get(this.id);
			if (result.sampleCount == 0)
				return Double.POSITIVE_INFINITY;
			else
				return result.value + gamma * result.iidStandardError;
		}
	}

	public VBRAdaVar(double delta, UncertaintyType utype, SelectionType seltype) {
		this.utype=utype;
		this.seltype=seltype;
		if (delta < 0)
			throw new IllegalArgumentException("delta must be non-negative.");
		this.delta = delta;
	}

	public enum UncertaintyType { LALITHA, TRUE_VARIANCE, EMP_VARIANCE };

	public enum SelectionType { GREATESTUNCERTAITY, SMALLESTUNCERTAITY, RANDOM };

	@Override
	protected Result body() {

		sdMultiplier=2;
		final int K = samplers.size();
		SortedSet<Candidate> queue = new TreeSet<>(comparatorByUncertainty.thenComparing(comparatorById));
		Collection<Candidate> arms = new HashSet<>();




		// The minimum # of samples per arm for this algorithm
		int minimumSamples = (int) Math.ceil(4 * Math.log(1/delta) + 1);
		if (budget < K * minimumSamples)
			throw new RuntimeException("This algorithm needs at least " + minimumSamples + " samples per arm to work.");
		pullAllArms(minimumSamples);
		// Fill the queue
		for (int i=0; i<K; i++) {
			Candidate c = new Candidate();
			c.sampler = samplers.get(i);
			c.id = i;
			updateUncertainty(c);
			queue.add(c);
			arms.add(c);
		}







		// System.err.println("Uniformemente:" + (K * minimumSamples));

		if (Globals.debug()) {
			System.out.println("VBRAV number of initial pulls=" + minimumSamples);
			System.out.println("candidates= ");
			for (VBRAdaVar.Candidate c : queue) {
				System.out.println(c.id + " " + results.get(c.id).sampleCount +" " + c.getUpperBound(2) + "\n " + c.uncertainty +  "\t"+ results.get(c.id).iidStandardError);

			}
		}

		final int residualBudget = budget - K * minimumSamples;
		int t = 0;
		Candidate cutThresholdOwner = null;
		double cutThreshold = Double.NEGATIVE_INFINITY;
		//double sdMultiplier = 2; // aka gamma


		while (queue.size() > 1 && t < residualBudget) {

			if (Globals.debug())
				System.out.println("VBRAV t=" + t + "\t functions " + queue.size());

			// Selection
			Candidate current = null;
			do { // Early cut
				current=selectCandidate(queue);

				queue.remove(current);
			} while (!queue.isEmpty() && current.getUpperBound(sdMultiplier) < cutThreshold);

			if (!queue.isEmpty()) {
				// Pull
				pull(current.id, 1);
				t++;
				Summary result=results.get(current.id);
				if (Globals.debug()) {

					System.out.println("f" + current.id + "\t val " + result.value
							+ "\t sd " + result.standardError
							+ "\t simpleSd " + result.iidStandardError);
				}


				updateUncertainty(current);


				if (Globals.debug())
					System.out.println("Current =" + current.id + "\t  " + current.uncertainty);

				// System.err.println("Arm:" + current.id + "\tempVar:" + empiricalVariance + "\t" + current.result.sampleCount + "\tQueue:" + queue.size());
				// System.err.println("\t" + current.uncertainty);
				// double stdErr = Math.sqrt(current.uncertainty);

				double lowerBound = current.getLowerBound(sdMultiplier);
				double upperBound = current.getUpperBound(sdMultiplier);
				// Raise the cut threshold
				if (lowerBound > cutThreshold) {
					cutThresholdOwner = current;
					cutThreshold = lowerBound;
				}

				// Lower the cut threshold
				if (cutThresholdOwner == current && lowerBound < cutThreshold) {
					// Find the new maximum lower bound
					cutThresholdOwner = Collections.max(queue, comparatorByLowerBound(sdMultiplier));
					cutThreshold = cutThresholdOwner.getLowerBound(sdMultiplier);
				}

				// Late cut
				if (upperBound >= cutThreshold) {
					queue.add(current);
				}
			}

			 //Catch-up step: we still have budget, but at most 1 candidate
			if (queue.size() <= 1 && t < residualBudget) {
				sdMultiplier *= 1.5;

				queue.addAll(arms);
				cutThresholdOwner = Collections.max(queue, comparatorByLowerBound(sdMultiplier));
				cutThreshold = cutThresholdOwner.getLowerBound(sdMultiplier);
			}
		}
		// System.err.print("\tNon usati:" + (residualBudget - t) + "\t in coda:" + queue.size());
		return theMostPromising();
	}


	Candidate selectCandidate(SortedSet<Candidate> queue){
		Random rand=new Random();
		Candidate selected;
		switch (seltype) {
			case GREATESTUNCERTAITY:
				selected= queue.last();
				break;
			case SMALLESTUNCERTAITY:
				selected= queue.first();
				break;
			default:
				int draw=rand.nextInt(queue.size());
				selected=(Candidate) queue.toArray()[draw];
				break;

		}
		return selected;

	}

	private void updateUncertainty(Candidate current) {
		Summary result=results.get(current.id);
		switch (utype) {
			case TRUE_VARIANCE:
				double trueVariance = moments.get(current.id).sd * moments.get(current.id).sd;
				current.uncertainty = trueVariance / result.sampleCount;
				break;
			case EMP_VARIANCE:
				current.uncertainty = result.iidStandardError;
				break;
			case LALITHA:
				double empiricalVariance = result.iidStandardError * result.iidStandardError * result.sampleCount;
				// This is U/N in the paper by Lalitha et al.
				current.uncertainty =
						empiricalVariance /
								(result.sampleCount * (1 - 2*Math.sqrt(Math.log(1.0/delta)/(result.sampleCount-1))));
				break;
		}
	}


	@Override
	public String getTag() {
		switch (seltype) {
			case GREATESTUNCERTAITY:
                return "VBRAV";
			case SMALLESTUNCERTAITY:
				return "VBRAVI";
			default:
				return "VBRAVR";

		}

	}
}
