package mab.finder;

import mab.*;
import integrate.Summary;

import java.util.*;

/** The adaptive UCB-E algorithm from Audibert et al. (COLT 2010),
 *  Figure 4.
  */
public class AdaptiveUCBE extends AbstractMaxFinder
{
    private int[] sampleCount;
    private double explRate;
    private Set<Candidate> allArms = new HashSet<>();

    private class Candidate  {
        Summary result;
        int id;
        double ucb;
    }

    /**
     @param explRate The exploration rate (called c in Audibert et al., COLT 2010, Figure 4).
     */
    public AdaptiveUCBE(double explRate) {
        this.explRate = explRate;
    }

    private void initPhaseAllocation() {
        this.sampleCount = new int[K];

        double log = 0.5;
        for (int i=2; i<=K; i++)
            log += 1/(double)i;
        for (int phase=1; phase<K; phase++) {
            sampleCount[phase] = (int) Math.ceil((budget-K) / ((K + 1 -phase) * log));
            if (Globals.debug())
                System.out.println("Adaptive UCBE sampleCount[" + phase + "] " + sampleCount[phase]);
        }
    }

    @Override
    protected Result body() {
        initPhaseAllocation();

        SortedSet<Candidate> queue =
                new TreeSet<Candidate>(Comparator.<Candidate>comparingDouble(c->c.ucb).thenComparingInt(c->c.id));

        // Fill the queue
        for (int i=0; i < K; i++) {
            Candidate c = new Candidate();
            c.id = i;
            c.result = new Summary(Double.NEGATIVE_INFINITY);
            c.ucb = Double.POSITIVE_INFINITY;
            queue.add(c);
        }
        allArms.clear();
        allArms.addAll(queue);

        int round = 0, maxRounds = K-1;

        double aFact, hFact;
        // For each round
        while (round < maxRounds) {

            int budgetPerRound = (sampleCount[round + 1] - sampleCount[round]) * (K - round);

            if (Globals.debug())
                System.out.println("round " + round + "\t functions " + queue.size() + " \t budgetPerRound" + budgetPerRound);

            // Update ucb values
            hFact = calculateHFactor(round);
            aFact = (this.explRate * budget) / hFact;
            //System.out.println("hFact " + hFact);

            queue.clear();
            for (Candidate candidate : allArms) {
                candidate.ucb = candidate.result.value + Math.sqrt(aFact / candidate.result.sampleCount);
                queue.add(candidate);
            }

            for (int t=0; t < budgetPerRound; t++) {
                Candidate maxCandidate = queue.last();
                maxCandidate.result = pull(maxCandidate.id, 1);
                double newUcb = maxCandidate.result.value + Math.sqrt(aFact / maxCandidate.result.sampleCount);
                queue.remove(maxCandidate);
                maxCandidate.ucb = newUcb;
                queue.add(maxCandidate);
            }
            round++;
        }
        return theMostPromising();
    }

    private double calculateHFactor(int round){
        if(round==0) return K;

        Candidate topCandidate = Collections.max(allArms, Comparator.comparingDouble(c->c.result.value));

        double max = topCandidate.result.value;

        double[] regr = new double[K];
        int i=0;
        for (Candidate candidate: allArms) {
            regr[i] = max - candidate.result.value;
            i++;
        }
        Arrays.sort(regr);

        double hFactor = Double.NEGATIVE_INFINITY;
        //update regret with i*\Delta_i^{-2}
        for (int j=K-round;j<K;j++) {
            regr[j] = (j+1) / (Math.pow(regr[j], 2));
            if(regr[j]>hFactor)
                hFactor=regr[j];
        }
        return hFactor;
    }

    @Override
    public String getTag() { return "OUCBE"; }
}
