package mab.finder;

import mab.Result;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.Well19937c;


public class UnivThompsonSampling extends AbstractMaxFinder{
    private static final Well19937c randomGenerator=new Well19937c();
    private static final double samplingAccuracy = 1E-3;

    @Override
    protected Result body() {
        if (budget<2*K) throw new RuntimeException("Insufficient budget.");

        pullAllArms(2);
        int runningBudget = budget - 2*K;

        NormalDistribution[] normals = new NormalDistribution[K];
        for (int arm=0; arm<K; arm++) {
            if (results.get(arm).iidStandardError>0)
                normals[arm] = new NormalDistribution(randomGenerator, results.get(arm).value, results.get(arm).iidStandardError, samplingAccuracy);
        }

        for(;runningBudget>0; runningBudget--){
            int leader=-1;
            double maxMean = Double.NEGATIVE_INFINITY;
            for(int arm=0;arm<K;arm++){
                double sample;
                if (results.get(arm).iidStandardError>0) {
                    sample = normals[arm].sample();
                } else {
                    sample = results.get(arm).getValue();
                }
                if (sample>maxMean) {
                    maxMean = sample;
                    leader = arm;
                }
            }
            pull(leader, 1);
            if (results.get(leader).iidStandardError > 0)
                normals[leader] = new NormalDistribution(randomGenerator, results.get(leader).value, results.get(leader).iidStandardError, samplingAccuracy);
        }
        return theMostPromising();
    }


    @Override
    public String getTag() { return "UnivThom"; }
}
