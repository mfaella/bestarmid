package mab.finder;

import mab.Result;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

/** Boltzmann exploration with user-provided constant temperature.
 *
 *  A.k.a. "Boltzmann done wrong" according to Cesa-Bianchi et al., NIPS 2017.
 */
public class Boltzmann extends AbstractMaxFinder {

    private final int phases;
    private final double temperature;

    public Boltzmann(int phases, double temperature) {
        if (phases <= 0)
            throw new IllegalArgumentException("Cannot work with 0 phases");

        this.phases = phases;
        this.temperature = temperature;
    }

    @Override
    protected Result body() {
        int[] index = new int[K];
        for (int i =0;i<K;i++) {
            index[i] = i;
        }
        pullAllArms(1);
        budget -= K;
        int phaseBudget = budget / (phases - 1);

        for (int phase=1; phase<phases; phase++) {
            double[] probabilities = boltzmann();
            EnumeratedIntegerDistribution preferenceDistribution = new EnumeratedIntegerDistribution(index, probabilities);
            pullAccordingToDistribution(preferenceDistribution, phaseBudget);
        }
        return theMostPromising();
    }

    private double[] boltzmann() {
        double[] probs = new double[K];
        double total = 0;
        for (int i=0; i<K; i++) {
            total += Math.exp(results.get(i).value / temperature);
        }
        for (int i=0; i<K; i++) {
            probs[i] = Math.exp(results.get(i).value / temperature) / total;
        }
        return probs;
    }

    @Override
    public String getTag() { return "Boltz"; }
}
