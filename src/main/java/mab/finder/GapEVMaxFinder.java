package mab.finder;

import mab.*;
import integrate.Summary;

import java.util.*;

/** The Adaptive GapE-V algorithm of Gabillon et al., NIPS 2011. */
public class GapEVMaxFinder extends AbstractMaxFinder
{
    private final double eta; // Parameter to adaptive Hsigma.
    private final double b;   // Upper bound. RVs should be bounded in [0,b]

    private static class Candidate  {
        Summary result;
        int id;
		double gap;
		double variance;
    }

    /**
     	@param eta
	 	@param ub	Upper bound to the rewards.
	 */
    public GapEVMaxFinder(double eta, double ub) {
		this.eta = eta;
		this.b = ub;
    }

	/**
	 	@param ub	Upper bound to the rewards.
	 */
	public GapEVMaxFinder(double ub) {
		this(1, ub);
	}


	@Override
    protected Result body() {

        final double INITIAL_ESTIMATE = Double.POSITIVE_INFINITY;

        // Fill in the set of candidates
		final List<Candidate> candidates = new ArrayList<>(K);
        for (int i=0; i<K; i++) {
            Candidate c = new Candidate();
            c.id = i;
            c.result = null;
            candidates.add(c);
        }
		final int bootstrapRounds = 2;
		int bootstrapIndex = 0;
	
	// Main loop
	for (int t=0; t<budget; t++) {

	    Candidate maxPotentialCandidate = null;			

	    if (t< bootstrapRounds * K) {
			maxPotentialCandidate = candidates.get(bootstrapIndex);
			bootstrapIndex = (bootstrapIndex + 1) % K;
	    } else {
		// Find highest and second highest values (needed for gaps)
			double maxValue = Double.NEGATIVE_INFINITY;
			double secondMaxValue = Double.NEGATIVE_INFINITY;
			Candidate maxValueCandidate = null;
			for (Candidate c: candidates) {
		    	double value = c.result.value;
		    	if (value > maxValue) {
					secondMaxValue = maxValue;
					maxValue = value;
					maxValueCandidate = c;
		    	} else if (value > secondMaxValue)
					secondMaxValue = value;
			}

		assert maxValueCandidate != null;
		assert maxValue > Double.NEGATIVE_INFINITY;
		assert secondMaxValue > Double.NEGATIVE_INFINITY;

		// Update gaps and estimated variance
		for (Candidate c: candidates) {
		    if (c == maxValueCandidate)
				c.gap = Math.abs(secondMaxValue - c.result.value);
		    else
				c.gap = Math.abs(maxValue - c.result.value);
		    c.variance = (c.result.iidStandardError * c.result.iidStandardError) * c.result.sampleCount;
		}
		
		// Adapt exploration rate to current estimate for H^sigma
		double Hsigma =  estimateHsigma(candidates);
		double a = this.eta * this.budget / Hsigma;

		if (Globals.debug())
		    System.out.println("estimated Hsigma " + Hsigma + "\t a " + a);
		
		// Find highest potential (B_mk in the paper)
		double maxPotential = Double.NEGATIVE_INFINITY;			
		for (Candidate c: candidates) {
		    // GapE:
		    // double potential = -gap + b * Math.sqrt(explRate / c.result.sampleCount);
		    // GapE-V:
		    int sampleCount = c.result.sampleCount;
		    double potential = - c.gap 
			+ Math.sqrt(2 * a * c.variance / sampleCount)				
			+ 7 * a * this.b / (3 * (sampleCount - 1));
		    // System.out.println(potential);
		    if (potential>maxPotential) {
				maxPotential = potential;
				maxPotentialCandidate = c;
		    }
		}
		//if (maxPotentialCandidate == null)
		//    maxPotentialCandidate = candidates.get(0);
	    }
	    if (Globals.debug())
			System.out.println("round " + t + "\t maxPotentialCandidate " + maxPotentialCandidate.id);

	    // Now sample maxPotentialCandidate
		maxPotentialCandidate.result = pull(maxPotentialCandidate.id, 1);

	} // End main loop
		return theMostPromising();
    }


    private double estimateHsigma(Collection<Candidate> candidates) {
	double Hsigma = 0;
	for (Candidate c: candidates) {
	    int sampleCount = c.result.sampleCount;
	    if (sampleCount<1) sampleCount = 1;
	    double LCB = c.variance - Math.sqrt(2.0 / (sampleCount-1)),
		UCB = c.gap + Math.sqrt(1 / (2.0 * sampleCount));
	    if (LCB < 0) LCB = 0;
	    double temp =  (LCB + Math.sqrt(LCB*LCB + (16/3.0)* this.b * UCB)); //divisione intera invece che float
	    if (Globals.debug())
		System.out.println("LCB: " + LCB + "\t UCB: " + UCB);
	    Hsigma += (temp * temp) / (UCB * UCB);
	}
	return Hsigma;
    }
    
    @Override
    public String getTag() { return "GapEV"; }
}





