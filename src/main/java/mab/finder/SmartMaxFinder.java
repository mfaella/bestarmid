package mab.finder;

import mab.*;
import integrate.Summary;
import mab.sampler.Sampler;

import java.util.*;
import java.util.function.IntBinaryOperator;
import java.util.function.ToDoubleFunction;

/**
 * The MaxFinder based on the fixed-budget VBR algorithm (ECAI 2020).
 * The standard factory method is newFixedBudgetMCMC.
 */
public class SmartMaxFinder extends AbstractMaxFinder
{
    // Inputs
    private double sdMultiplier;

    // Internal
    private int initialBudgetPerFunction;
    private int maxRounds;
    private IntBinaryOperator budgetPerFunctionUpdater;

    private Runnable initializer;
    private ToDoubleFunction<Summary> errorExtractor;
    private int totalBudget;
    private final boolean saturateBudget = true;
    
    private static final Comparator<Candidate> comparatorByValue =
	Comparator.comparingDouble(cand -> cand.result.value);

	private class Candidate implements Comparable<Candidate> {
		Summary result;
		Sampler sampler;
		int id;

		/* Sorted by lower confidence bound (value - scaled error), then by id */
		@Override
		public int compareTo(Candidate other) {
			int valueCmp = (int) Math.signum((result.value - errorExtractor.applyAsDouble(result) * sdMultiplier) -
					(other.result.value - errorExtractor.applyAsDouble(other.result) * sdMultiplier));
			// It must be a *linear* order for TreeSet to work
			if (valueCmp != 0)
				return valueCmp;
			else
				return id - other.id;
		}
	}

    private SmartMaxFinder(double sdMultiplier) {
		if (sdMultiplier < 0)
	    	throw new IllegalArgumentException("sdMultiplier must be non-negative.");
		this.sdMultiplier = sdMultiplier;
    }

    /**
       This (old) version doubles the number of samples per function at each round.
       It's not a real fixed-budget algorithm, because the total number of samples used
       depends on how many functions are discarded in each round.

       @param budget The maximum number of samples that can be requested for each function.
     */
	/*
    public static SmartMaxFinder newWithPerFunctionBudget(List<Sampler> samplers, List<Function> functions, int budget, double sdMultiplier) {
	if (budget <= 0)
	    throw new IllegalArgumentException("Cannot work with budget " + budget + "");

	SmartMaxFinder finder = new SmartMaxFinder(samplers, functions, sdMultiplier);
	// This parameter is heuristic, other values may be better
	finder.maxRounds = 5;
	final int initialBudgetPerFunction = budget / ((1 << finder.maxRounds) -1);
	finder.initialBudgetPerFunction = initialBudgetPerFunction;
	// This version doubles the number of samples at each round
	finder.budgetPerFunctionUpdater = (round, _residualFunctionCount) ->
	    initialBudgetPerFunction * (1 << round);
	finder.errorExtractor = result -> result.standardError;
	finder.compute();
	return finder;
    }
*/

    /**
       This (old) version doubles the number of samples per function at each round,
       and then it distributes it among the remaining functions.

       @param budget The maximum number of samples that can be requested over all functions.
     */
	/*
    public static SmartMaxFinder newWithOverallBudget(List<Sampler> samplers, List<Function> functions,
						      int budget, double sdMultiplier) //mai usata
    {
	if (budget <= 0)
	    throw new IllegalArgumentException("Cannot work with budget " + budget + "");

	SmartMaxFinder finder = new SmartMaxFinder(samplers, functions, sdMultiplier);
	finder.maxRounds = 5;
	finder.totalBudget = budget;
	final int initialBudgetPerFunction = budget / ((1 << finder.maxRounds) -1);
	finder.initialBudgetPerFunction = initialBudgetPerFunction / functions.size();
	// Twice as many samples as the previous round, to divide among remaining candidates
	finder.budgetPerFunctionUpdater = (round, residualFunctionCount) ->
	    initialBudgetPerFunction * (1 << round) / residualFunctionCount;
	finder.errorExtractor = result -> result.standardError;
	finder.compute();
	return finder;
    }
*/

    /**
       @param budget The maximum number of samples that can be requested over all functions.
     */
	/*
    public static SmartMaxFinder newFixedBudgetIidTolerant(List<Sampler> samplers, List<Function> functions,
							   int budget, double sdMultiplier) //mai usata
    {
	if (budget <= 0)
	    throw new IllegalArgumentException("Cannot work with budget " + budget + "");
	SmartMaxFinder finder = new SmartMaxFinder(samplers, functions, sdMultiplier);

        final int K = functions.size();
        int[] sampleCount = new int[K];
	double log = 0.5;
	for (int i=2; i<=K; i++)
	    log += 1/(double)i;
	for (int phase=1; phase<K; phase++) {
	    sampleCount[phase] = (int) Math.ceil((budget-K) / ((K + 1 -phase) * log));
	}

	finder.maxRounds = K-1;
	finder.initialBudgetPerFunction = sampleCount[1] - sampleCount[0];
	finder.totalBudget = budget;
	finder.budgetPerFunctionUpdater = new IntBinaryOperator() {
                private int lastEliminated = 0;
                @Override
                public int applyAsInt(int round, int residualFunctionCount) {
                    int eliminated = K - residualFunctionCount;
                    if (eliminated <= lastEliminated) {
                        eliminated = lastEliminated + 1;
                    }
                    // Check if over budget
                    if (eliminated >= K-1) {
                        finder.maxRounds = 0;
                        return 0;
                    }
                    int nextBudget = 0;
                    for (int i=lastEliminated+1; i<=eliminated; i++)
                        nextBudget += (K - i) * (sampleCount[i+1] - sampleCount[i]);
                    nextBudget /= residualFunctionCount;
		    if (nextBudget == 0)
			nextBudget = 1;
                    lastEliminated = eliminated;
                    return nextBudget;
                }
            };
	finder.errorExtractor = result -> result.iidStandardError;
	finder.compute();
	return finder;
    }
*/
    
    /**
       It assumes that the samplers return iid samples, so that error estimation is simpler.
       Use on: samplers based on simple distributions that can be sampled directly (Gaussian).
       Do not use on: samplers based on MCMC.
     */
    public static SmartMaxFinder newFixedBudgetIid(double sdMultiplier)
    {
		SmartMaxFinder finder = new SmartMaxFinder(sdMultiplier);

		finder.initializer = () -> {
			int K = finder.K;
			int[] sampleCount = new int[K];
			double log = 0.5;
			for (int i = 2; i <= K; i++)
				log += 1 / (double) i;
			// The n_k vector from SR
			for (int phase = 1; phase < K; phase++) {
				sampleCount[phase] = (int) Math.ceil((finder.budget - K) / ((K + 1 - phase) * log));
			}
			finder.maxRounds = K-1;
			finder.initialBudgetPerFunction = sampleCount[1] - sampleCount[0];
			finder.budgetPerFunctionUpdater = new IntBinaryOperator() {
				private int lastEliminated = 0;
				@Override
				public int applyAsInt(int round, int residualFunctionCount) {
					int eliminated = finder.K - residualFunctionCount;
					if (eliminated <= lastEliminated) {
						throw new RuntimeException("No candidate was eliminated in this round.");
					}
					int nextBudget = 0;
					for (int i=lastEliminated+1; i<=eliminated; i++)
						nextBudget += (finder.K - i) * (sampleCount[i+1] - sampleCount[i]);
					nextBudget /= residualFunctionCount;
					lastEliminated = eliminated;
					return nextBudget;
				}
			};
		};
		finder.errorExtractor = result -> result.iidStandardError;
		return finder;
    }

    
    /**
       This is the current general-purpose implementation, as per ECAI 2020.
     */
    public static SmartMaxFinder newFixedBudgetMCMC(double sdMultiplier)
    {
		SmartMaxFinder finder = new SmartMaxFinder(sdMultiplier);

		finder.initializer = () -> {
			final int K = finder.K;
			int[] sampleCount = new int[K];
			double log = 0.5;
			for (int i = 2; i <= K; i++)
				log += 1 / (double) i;
			for (int phase = 1; phase < K; phase++) {
				sampleCount[phase] = (int) Math.ceil((finder.budget - K) / ((K + 1 - phase) * log));
			}

			finder.maxRounds = K - 1;
			finder.initialBudgetPerFunction = sampleCount[1] - sampleCount[0];
			finder.budgetPerFunctionUpdater = new IntBinaryOperator() {
				private int lastEliminated = 0;

				@Override
				public int applyAsInt(int round, int residualFunctionCount) {
					int eliminated = K - residualFunctionCount;
					if (eliminated <= lastEliminated) {
						eliminated = lastEliminated + 1;
					}
					// Check if over budget
					if (eliminated >= K - 1) {
						finder.maxRounds = 0;
						return 0;
					}
					int nextBudget = 0;
					for (int i = lastEliminated + 1; i <= eliminated; i++)
						nextBudget += (K - i) * (sampleCount[i + 1] - sampleCount[i]);
					nextBudget /= residualFunctionCount;
					if (nextBudget == 0)
						nextBudget = 1;
					lastEliminated = eliminated;
					return nextBudget;
				}
			};
		};
		finder.errorExtractor = result -> result.standardError;
		return finder;
    }

    /** Refines the value of the maximum up to the prescribed number of samples. */
    /*
    public void refineResult(int extraSampleCount) { //mai usata
    	Sampler sampler = samplers.get(maxIndex);
    	Function f = functions.get(maxIndex);
    	assert maxValue.lastSample != null;
    	List<double[]> samples = sampler.sample(extraSampleCount);
    	maxValue = Integral.refine(maxValue, f, samples);
    }
    */

	@Override
    protected Result body() {
		initializer.run();
    	TreeSet<Candidate> queue = new TreeSet<>(),
	                   nextQueue = new TreeSet<>(),
	                   oldQueue = null;

    	// Initial estimate is infinity, so candidate cannot be discarded
    	final double INITIAL_ESTIMATE = Double.POSITIVE_INFINITY;

    	// Fill the queue
    	for (int i=0; i<samplers.size(); i++) {
    		Candidate c = new Candidate();
    		c.sampler = samplers.get(i);
    		c.id = i;
    		c.result = new Summary(INITIAL_ESTIMATE);
    		queue.add(c);
    	}

    	// Samples per function in the current round
    	int budgetPerFunction = this.initialBudgetPerFunction;

    	Candidate maxCandidate = null;
    	// DEBUG
    	// System.out.println("initial sample count: " + budgetPerFunction);
    	int round = 0;

    	// For each round
    	while (queue.size() > 1 && round < this.maxRounds) {

    		if (Globals.debug())
    			System.out.println("Smart round " + round + "\t functions " + queue.size() + " \t budgetPerFunction " + budgetPerFunction);

    		if (budgetPerFunction==0) {
    			// If there's no budget, just drop the least promising candidate
    			nextQueue.addAll(queue);
    			Candidate leastLikely = Collections.min(queue, comparatorByValue);
    			nextQueue.remove(leastLikely);
    		} else {	    
    			Iterator<Candidate> iter = queue.descendingIterator();
    			// reset at each stage
    			double cutThreshold = Double.NEGATIVE_INFINITY;
    			double max = Double.NEGATIVE_INFINITY;
    			int toBeSampled = queue.size();

    			// For each function
    			while (iter.hasNext()) {
    				Candidate current = iter.next();

    				final double oldUpperBound = current.result.value + errorExtractor.applyAsDouble(current.result) * sdMultiplier;
				// Early cut
    				if (oldUpperBound < cutThreshold) {
    					// Discard before update
    					// DEBUG
    					// System.out.println("[a:" + ((cutThreshold-current.result.value)/current.result.sd) + "]");
    					// Reassign budget to remaining functions in this round
    					budgetPerFunction += budgetPerFunction / toBeSampled;
    					continue;
    				}

    				toBeSampled--;
				current.result = pull(current.id, budgetPerFunction);

    				if (Globals.debug())
    					System.out.println("f" + current.id + "\t val " + current.result.value
    							+ "\t sd " + current.result.standardError
    							+ "\t simpleSd " + current.result.iidStandardError
    							+ "\t error " + errorExtractor.applyAsDouble(current.result));

    				// Update max (eventual output)
    				if (current.result.value > max) {
    					max = current.result.value;
    					maxCandidate = current;
    				} else {
    					// Late cut
    					final double upperBound = current.result.value + errorExtractor.applyAsDouble(current.result) * sdMultiplier;
    					if (upperBound < cutThreshold) {
					    continue;
    					}
    				}
    				// Add to next stage
    				nextQueue.add(current);

    				// Update cut threshold
    				final double lowerBound = current.result.value
    						- errorExtractor.applyAsDouble(current.result) * sdMultiplier;
    				if (lowerBound > cutThreshold) {
    					cutThreshold = lowerBound;
    				}
    			} // end of round (end inner while)
			
			// Discard at least one
			if (nextQueue.size() == queue.size()) {
			    Candidate leastLikely = Collections.min(nextQueue, comparatorByValue);
			    nextQueue.remove(leastLikely);
			}
    		} // end if 

		// Experimental: discard only at the end of a phase
		// filterQueue(queue);
		
    		oldQueue = queue;
    		queue = nextQueue;
    		nextQueue = new TreeSet<>();
    		round++;
    		if (queue.size()>1)
    			budgetPerFunction = this.budgetPerFunctionUpdater.applyAsInt(round, queue.size());
    	} // end outer while (for each round)

    	// Saturation (aka catch-up phase)
    	if (saturateBudget && budgetPerFunction > 0) {
	    if (Globals.debug())
		System.out.println("Saturating");
	    int residualBudget = budget - totalSampleCount;
	    budgetPerFunction = residualBudget / oldQueue.size();	    
	    double max = Double.NEGATIVE_INFINITY;
	    for (Candidate current: oldQueue) {
		current.result = pull(current.id, budgetPerFunction);
		// Update max (eventual output)
		if (current.result.value > max) {
		    max = current.result.value;
		    maxCandidate = current;
		}
	    }
	}
	// System.err.println(experimentCounter + " VBR alloc: " + Arrays.toString(allocation));
	return thisArm(maxCandidate.id);
    }

    @Override
    public String getTag() { return "VBR"; }

    private void filterQueue(SortedSet<Candidate> queue) {
	Candidate maxLowerBoundCandidate = queue.last();
	double maxLowerBound = maxLowerBoundCandidate.result.value
	    - errorExtractor.applyAsDouble(maxLowerBoundCandidate.result) * sdMultiplier;
	Iterator<Candidate> iter = queue.iterator();
	while (iter.hasNext()) {
	    Candidate c = iter.next();
	    double upperBound = c.result.value + errorExtractor.applyAsDouble(c.result) * sdMultiplier;
	    if (upperBound < maxLowerBound)
		iter.remove();
	}
    }
}
