package mab.finder;

import java.util.*;

import mab.*;
import integrate.Summary;
import mab.sampler.Sampler;
import org.apache.commons.math3.distribution.*;

public class SRLeastLikely extends AbstractMaxFinder
{
    private int[] sampleCount;

        /** Returns the probability that the first Gaussian RV is greater than the second. */
    private static double probFirstGreaterThanSecond(double mean1, double sd1, double mean2, double sd2) {
		double mean = mean1 - mean2;
		double sd = Math.sqrt(sd1*sd1 + sd2*sd2);
		NormalDistribution d = new NormalDistribution(null, mean, sd);
		return 1 - d.cumulativeProbability(0);
    }

    /** Returns the probability that the first Gaussian RV is greater than the second. */
    private static double probGreaterThan(double mean, double sd, double value) {
		NormalDistribution d = new NormalDistribution(null, mean, sd);
		return 1 - d.cumulativeProbability(value);
    }

    private static class Candidate {
		Summary result;
		int id;
		double probOfBeingMax;

		void updateProbOfBeingMax(Iterable<Candidate> candidates) {
			probOfBeingMax = 1;
			double mySd = result.standardError;
			for (Candidate other : candidates) {
				if (other == this) continue;
				double mean = result.value - other.result.value;
				double otherSd = other.result.standardError;
				double sd = Math.sqrt(mySd * mySd + otherSd * otherSd);
				NormalDistribution d = new NormalDistribution(mean, sd);
				probOfBeingMax *= 1 - d.cumulativeProbability(0);
			}
		}

		void updateProbOfBeingMax(Candidate mostLikelyMax) {
			double mySd = result.standardError;
			double otherSd = mostLikelyMax.result.standardError;
			probOfBeingMax = probFirstGreaterThanSecond(result.value, mySd, mostLikelyMax.result.value, otherSd);
			// probOfBeingMax = probGreaterThan(result.value, mySd, mostLikelyMax.result.value);
		}

		static final Comparator<Candidate> byValue =
				Comparator.comparingDouble(cand -> cand.result.value);
		static final Comparator<Candidate> byProbOfBeingMax =
				Comparator.comparingDouble(cand -> cand.probOfBeingMax);

		@Override
		public String toString() {
			return "f" + id + "\t val " + result.value + "\t prob " + probOfBeingMax;
		}
	}

	@Override
	protected Result body() {

		this.sampleCount = new int[K];
		double log = 0.5;
		for (int i=2; i<=K; i++)
			log += 1/(double)i;
		for (int phase=1; phase<K; phase++) {
			sampleCount[phase] = (int) Math.ceil((budget-K) / ((K + 1 -phase) * log));
			if (Globals.debug())
				System.out.println("SR sampleCount[" + phase + "] " + sampleCount[phase]);
		}

		final double INITIAL_ESTIMATE = Double.POSITIVE_INFINITY;

		Set<Candidate> queue = new HashSet<>();
		// Fill the queue
		for (int i=0; i<samplers.size(); i++) {
			Candidate c = new Candidate();
			c.id = i;
			c.result = new Summary(INITIAL_ESTIMATE);
			queue.add(c);
		}

		int round = 0, maxRounds = samplers.size()-1;

		// For each round
		while (round < maxRounds) {

			int budgetPerFunction = sampleCount[round+1] - sampleCount[round];

			if (Globals.debug())
				System.out.println("round " + round + "\t functions " + queue.size() + " \t budgetPerFunction " + budgetPerFunction);

			if (budgetPerFunction > 0) {

				// For each function
				for (Candidate candidate: queue) {

					if (Globals.debug())
						System.out.println("Updating f" + candidate.id);

					candidate.result = pull(candidate.id, budgetPerFunction);

					if (Globals.debug())
						System.out.println("val " + candidate.result.value
								+ "\t sd " + candidate.result.standardError
								+ "\t simpleSd " + candidate.result.iidStandardError);
				} // end of round (end inner while)

				Candidate mostLikely = Collections.max(queue, Candidate.byValue);
				for (Candidate candidate: queue) {
					candidate.updateProbOfBeingMax(mostLikely);
				}
			}
			// TreeSet<Candidate> orderedCandidates = new TreeSet<>(queue);
			// if (Globals.debug())
			//    System.out.println(orderedCandidates);
			Candidate leastLikely = Collections.min(queue, Candidate.byProbOfBeingMax);

			// DEBUG
			Candidate worstValue = Collections.min(queue, Candidate.byValue);
			if (Globals.debug() && leastLikely != worstValue)
				System.out.println("ll: " + leastLikely + "\t wv: " + worstValue);

			if (Globals.debug())
				System.out.println("Discarding " + leastLikely);
			queue.remove(leastLikely);
			round++;
		} // end outer while

		if (queue.size() != 1)
			throw new AssertionError("Internal error");
		return thisArm(queue.iterator().next().id);
	}

    @Override
    public String getTag() { return "SR-LL"; }
}
