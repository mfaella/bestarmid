package mab.finder;

import mab.Result;
import mab.sampler.Sampler;
import mab.util.Moments;
import mab.util.OptimalAllocator;

import java.util.Arrays;
import java.util.List;


/** This finder knows the ground truth and attempts to find the
    optimal _non-adaptive_ pull allocation that maximizes the probability
    that the best arm ends with the highest reward estimate. 

    It uses a non-linear optimization algorithm provided by Apache Commons Math.
*/
public class OptimalMaxFinder extends FixedAllocationMaxFinder implements CheatingFinder
{
    private static final boolean DEBUG = false;

    private List<Moments> groundTruth;

    @Override
    public void setGroundTruth(List<Moments> groundTruth) {
	this.groundTruth = groundTruth;
    }

    @Override
    protected Result body() {
	if (groundTruth==null)
	    throw new IllegalStateException("Must call setGroundTruth before.");
	setAllocation(makeAllocation(groundTruth, budget));
	return super.body();
    }
    
    private static int[] makeAllocation(List<Moments> groundTruth, int budget) {
	OptimalAllocator allocator = new OptimalAllocator(groundTruth, budget);
	if (DEBUG) {
	    // System.err.println("Budget: " + budget + "\tsize: " + groundTruth.length);
	    System.err.println(groundTruth);
	    System.err.println(Arrays.toString(allocator.getDoubleAllocation()));
	    System.err.println(Arrays.toString(allocator.getAllocation()));
	}
	return allocator.getAllocation();
    }

    @Override
    public String getTag() { return "Opt"; }
}
