package mab;

import integrate.Summary;
import java.util.List;

public class Result {
    public int maxIndex;
    public double maxValue;
    public int totalSampleCount;
    public double totalReward;
    public int[] allocation;
    public List<Summary> details;

    public Result(int maxIndex, double maxValue, int totalSampleCount, double totalReward, int[] allocation, List<Summary> details) {
        this.maxIndex = maxIndex;
        this.maxValue = maxValue;
        this.totalSampleCount = totalSampleCount;
        this.totalReward = totalReward;
        this.allocation = allocation;
        this.details = details;
    }

    public double allocationEntropy() {
	double result = 0;
	for (int a: allocation) {
	    double p = a / (double) totalSampleCount;
	    result -= p * Math.log(p);
	}
	return result;
    }
}
