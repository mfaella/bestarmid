package mab.util;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.*;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer;
import org.apache.commons.math3.random.MersenneTwister;

import java.util.Arrays;
import java.util.List;

/**
 * Computes an approximately optimal sample allocation
 * given the budget and the true means and stddev.
 *
 * It tries to minimize the probability that, after sampling,
 * one of the non-optimal arms exhibits an empirical mean that is higher
 * than the one of the optimal arm.
 */
public class OptimalAllocator {
    // Inputs
    private final int budget;
    private final List<Moments> moments;
    private int maxId;
    private double maxMean;

    // Output
    private PointValuePair result;

    private static final boolean DEBUG = false;

    public OptimalAllocator(List<Moments> groundTruth, int budget) {
        this.moments = groundTruth;
        this.budget = budget;
        maxMean = Double.NEGATIVE_INFINITY;
        for (int i=0; i<groundTruth.size(); i++) {
            if (groundTruth.get(i).mean > maxMean) {
                maxMean = groundTruth.get(i).mean;
                maxId = i;
            }
        }
        compute();
    }

    private class ErrorFunction implements MultivariateFunction {
        private final int n;
        private final double[] delta;

        private ErrorFunction() {
            n = moments.size();
            delta = new double[n];
            for (int i=0; i<n; i++)
                delta[i] = maxMean - moments.get(i).mean;
        }

        public double error(double[] vars) {
            double val = 0;
            double sdOptimalArm = moments.get(maxId).sd / Math.sqrt(vars[maxId]);

            for (int i = 0; i < n; i++) {
                if (i == maxId)
                    continue;
                double sd = sdOptimalArm + moments.get(i).sd / Math.sqrt(vars[i]);

                // Fast but approximate
                val += NormalCumulativeProbability.eval(delta[i], sd, 0);

                // Slow but more precise
                //NormalDistribution differenceDistribution = new NormalDistribution(delta[i], sd);
                //val += differenceDistribution.cumulativeProbability(0);
            }
            return val;
        }

        @Override
        public double value(double[] vars) {
            double val = error(vars);

            // Apply penalty
            double sum = Arrays.stream(vars).sum();
            // relative budget overflow
            double overflow = sum > budget? (sum-budget)/budget : 0;
            val = val * (1+overflow*overflow);

            // System.err.println(val);
            return val;
        }
    }

    public static double[] uniformAllocation(int n, int budget) {
        double[] uniform = new double[n];
        Arrays.fill(uniform, budget / n);
        return uniform;
    }

    private void compute() {
        final int n = moments.size();

        // UNSUPPORTED - Sum of variables is equal to budget
        /*double[] coefficients = new double[n];
        Arrays.fill(coefficients, 1);
        LinearConstraint c = new LinearConstraint(coefficients, Relationship.LEQ, budget);
        LinearConstraintSet constraints = new LinearConstraintSet(c);
        */

        // Objective function
        ErrorFunction f = new ErrorFunction();
        ObjectiveFunction objfun = new ObjectiveFunction(f);

        // Simple bounds: each variable in [0, budget]
        double[] lower = new double[n], upper = new double[n];
        Arrays.fill(upper, budget);

        // Initial point: half the uniform allocation
        double[] initial = new double[n];
        Arrays.fill(initial, budget / (2*n));
        InitialGuess initialPoint = new InitialGuess(initial);

        // CMAES parameters
        double[] sigmas = new double[n];
        Arrays.fill(sigmas, budget/n);
        CMAESOptimizer.Sigma sigma = new CMAESOptimizer.Sigma(sigmas);
        CMAESOptimizer.PopulationSize pop = new CMAESOptimizer.PopulationSize(4 + (int)Math.ceil(3*Math.log(n)));

        // Ready to optimize
        // MultivariateOptimizer opt = new BOBYQAOptimizer(2*n, 1, 10);
        MultivariateOptimizer opt =
                new CMAESOptimizer(1000, 1E-3, true,
                        10, 10, new MersenneTwister(),
                        false, new SimpleValueChecker(1E-3, 1E-3));
        // MultivariateOptimizer opt = new PowellOptimizer(1E-3, 1E-3);

        // PowellOptimizer does not support simple constraints (bounds) or a linear constraint
        // BOBYQAOptimizer does not *seem* to support the linear constraint

        try {
            result = opt.optimize(pop, sigma, new SimpleBounds(lower, upper), objfun, initialPoint,
                    new MaxEval(10000), /*new MaxIter(100),*/ GoalType.MINIMIZE);
        } catch (TooManyEvaluationsException e) {
            result = new PointValuePair(initial, f.value(initial));
        }

        if (DEBUG) {
            System.err.println("Total: " + Arrays.stream(result.getPoint()).sum());
            System.err.println("Uniform allocation: error bound: " + f.error(uniformAllocation(n, budget))
                    + "\tobj fun: " + f.value(uniformAllocation(n, budget)));
            System.err.println("Initial allocation: error bound: " + f.error(initial)
                    + "\tobj fun: " + f.value(initial));
            System.err.println("Optimal allocation: error bound: " + f.error(result.getPointRef())
                    + "\tobj fun: " + f.value(result.getPointRef()));
        }
    }

    public double[] getDoubleAllocation() {
        return result.getPoint();
    }

    public int[] getAllocation() {
        double sum = Arrays.stream(result.getPointRef()).sum();

        int[] allocation = new int[moments.size()];
        for (int i=0; i< moments.size(); i++) {
            allocation[i] = (int) Math.round( (result.getPointRef()[i]/sum) * budget );
            if (allocation[i] == 0)
                allocation[i] = 1;
        }
        return allocation;
    }
}
