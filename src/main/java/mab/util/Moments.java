package mab.util;

public class Moments implements Comparable<Moments> {
    public double mean, sd;

    public Moments(double mean, double sd) {
        this.mean = mean;
        this.sd = sd;
    }

    public Moments() { }

    @Override
    public int compareTo(Moments other) {
        return Double.compare(mean, other.mean);
    }

    @Override
    public String toString() {
        return mean + " ±" + sd;
    }
}



