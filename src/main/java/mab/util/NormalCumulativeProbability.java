package mab.util;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well19937c;

public class NormalCumulativeProbability {
    private static final int MIN = -4, MAX = 4, RESOLUTION = 1_000_000;
    private static final double[] cumulative = new double[RESOLUTION],
                                  inverse = new double[RESOLUTION];

    static {
        long t0 = System.currentTimeMillis();
        init();
        t0 = System.currentTimeMillis() - t0;
        System.err.println("NormalCumulativeProbability init time = " + t0 + " millisecs");
    }

    private static void init() {
        NormalDistribution stdNormal = new NormalDistribution(null, 0, 1);
        for (int i=0; i<RESOLUTION; i++) {
            // x ranges from MIN to MAX
            double x = MIN + i*((MAX-MIN)/(double)(RESOLUTION-1));
            cumulative[i] = stdNormal.cumulativeProbability(x);
            // p ranges from 0 to 1
            double p = i * (1/(double)(RESOLUTION-1));
            inverse[i] = stdNormal.inverseCumulativeProbability(p);
        }
    }

    /** @return the cumulative probability P[X<x] for a r.v. X distributed as Normal(mean, sd). */
    public static double eval(double mean, double sd, double x) {
        double normalizedX = (x-mean)/sd;
        if (normalizedX < MIN) return cumulative[0]; // over-approx
        if (normalizedX > MAX) return 1;
        // i ranges from 0 to RESOLUTION-1
        int i = (int) (((normalizedX - MIN) / (MAX - MIN)) * (RESOLUTION-1));
	// Round instead of cast should be more precise
	//int i = (int) Math.round(((normalizedX - MIN) / (MAX - MIN) * (RESOLUTION-1)));
        return cumulative[i];
    }

    private static final RandomGenerator random = new Well19937c();

    public static double gaussianSample(double mean, double sd) {
        // between 0 and 1
        double p = random.nextDouble();
        int i = (int) (p * (RESOLUTION-1));
        double sample = inverse[i];
        return mean + sd * sample;
    }
}
