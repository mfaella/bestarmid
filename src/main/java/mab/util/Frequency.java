package mab.util;

import java.util.*;

import integrate.Density;
import gnu.trove.map.*;
import gnu.trove.map.hash.*;

public class Frequency {

    /** Given a function S->T and a list of S, it evaluates the function
	on each S and returns the frequency of each image T.
     */
    public static <S,T> Map<T,Double> multiCompute(java.util.function.Function<S,T> selector,
						   List<S> samples) {
	TObjectIntMap<T> internalMap = new TObjectIntHashMap<>();
	int n = 0;
	for (S sample: samples) {
	    T item = selector.apply(sample);
	    internalMap.adjustOrPutValue(item, 1, 1);
	    n++;
	}
	final int sampleCount = n;
	Map<T,Double> result = new HashMap<>();
	internalMap.forEachKey(item -> {
		result.put(item, internalMap.get(item)/(double)sampleCount);
		return true;
	    });
	return result;
    }

    /** Given a function double[]->T, a density, and a number of samples,
	it evaluates the function on each sample and returns the frequency of each image T.
     */
    public static <T> Map<T,Double> multiCompute(java.util.function.Function<double[],T> selector,
                                                 Density density, int sampleCount) {
    	if (sampleCount<=0)
            throw new IllegalArgumentException("Cannot estimate integral with no samples.");	
    	List<double[]> samples = density.sampler().sample(sampleCount);
	return multiCompute(selector, samples);
    }
}
