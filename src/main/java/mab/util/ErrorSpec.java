package mab.util;

public class ErrorSpec {
    private static final int UNDEFINED = -1;

    private final double absErr, relErr;
    private final int sampleCount;

    private ErrorSpec(double absErr, double relErr, int sampleCount) {
        this.absErr = absErr;
	    this.relErr = relErr;
        this.sampleCount = sampleCount;
    }

    public static ErrorSpec newFromAbsoluteError(double absErr) {
        return new ErrorSpec(absErr, UNDEFINED, UNDEFINED);
    }

    public static ErrorSpec newFromRelativeError(double relErr) {
        return new ErrorSpec(UNDEFINED, relErr, UNDEFINED);
    }

    public static ErrorSpec newFromSampleCount(int sampleCount) {
        return new ErrorSpec(UNDEFINED, UNDEFINED, sampleCount);
    }

    public int getSampleCount() {
        if (sampleCount == UNDEFINED)
            throw new IllegalStateException("Sample count undefined.");
        return sampleCount;
    }

    public double getAbsErr() {
	if (absErr == UNDEFINED)
            throw new IllegalStateException("Absolute error undefined.");
        return absErr;

    }
}
