package mab.util;

import java.util.Random;

public class Gumbel {
    private static final Random rand = new Random();

    /** Provides a sample from the standard Gumbel distribution (mu=0, beta=1). */
    public static double sample() {
        double u = rand.nextDouble();
        return - Math.log( - Math.log(u));
    }
}
