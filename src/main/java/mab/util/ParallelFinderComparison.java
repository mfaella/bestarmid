package mab.util;

import mab.MaxFinder;
import mab.Result;
import mab.sampler.Sampler;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;

public class ParallelFinderComparison {

    private String[] finderNames;
    private boolean verbose = true;
    private final int size;
	private final MaxFinder[] finders;
	private final double[] parameters;

	private final ExecutorService executor;
	// Stats
	private double[] errorCount, errorAmount;
	private long[] utilization;
	private AtomicInteger epochCount;

	public ParallelFinderComparison(MaxFinder[] finders, double[] parameters) {
		this(Math.max(1, Runtime.getRuntime().availableProcessors()-2), finders, parameters);
	}

	public ParallelFinderComparison(int parallelism, MaxFinder[] finders, double[] parameters) {
		executor = Executors.newFixedThreadPool(parallelism);
		this.finders = finders;
		this.parameters = parameters;
		size = finders.length;
		errorCount = new double[size];
		errorAmount = new double[size];
		utilization = new long[size];
		epochCount = new AtomicInteger();
		finderNames = new String[size];
		int i = 0;
		for (MaxFinder finder: finders) {
			finderNames[i++] = finder.getTag();
		}
		System.err.println("Performing experiment with " + size + " finders and " + parallelism + " threads");
	}

	private class Task implements Runnable {

		List<Sampler> arms;
		int budget;
		IntPredicate isTrueMax;
		double trueMaxVal;
		String message;
		int finderId;

		Task(List<Sampler> arms, int budget, IntPredicate isTrueMax,
			 double trueMaxVal, String message, int finderId) {
			this.arms = arms;
			this.budget = budget;
			this.isTrueMax = isTrueMax;
			this.trueMaxVal = trueMaxVal;
			this.message = message;
			this.finderId = finderId;
		}

		@Override
		public void run() {
			long start, stop;
			MaxFinder finder = finders[finderId];

			start = System.currentTimeMillis();
			Result res = finder.compute(arms, budget);
			stop = System.currentTimeMillis();
			long timeMillis = stop - start;

			int error = 0;
			double relError = 0;
			int max = res.maxIndex;
			double regret = res.totalSampleCount * trueMaxVal - res.totalReward;
			if (!isTrueMax.test(max)) {
				error = 1;
				errorCount[finderId]++;
				relError = Math.abs(res.maxValue - trueMaxVal) / trueMaxVal;
				errorAmount[finderId] += relError;
			}
			utilization[finderId] += res.totalSampleCount;
			if (verbose) {
				StringBuilder sb = new StringBuilder();
				sb.append(finder.getTag()).append("\t");
				if (parameters != null)
					sb.append(parameters[finderId]).append("\t");
				sb.append(timeMillis).append("\t");
				sb.append(error).append("\t");
				sb.append(relError).append("\t");
				sb.append(regret);
				sb.append(message);
				System.out.println(sb);
			}
		}
	}

    public void noDetailedOutput() {
		verbose = false;
    }

	public void addComparison(List<Sampler> arms, int budget, IntPredicate isTrueMax,
							  double trueMaxVal, String message) {
		Future<?>[] futures = new Future<?>[size];
		for (int i = 0; i < size; i++) {
			Task task = new Task(arms, budget, isTrueMax, trueMaxVal, message, i);
			futures[i] = executor.submit(task);
		}
		for (int i = 0; i < size; i++) {
			try {
				futures[i].get();
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void addComparison(List<Sampler> arms, int budget, int trueMax,
							  double trueMaxVal, String message)
	{
		addComparison(arms, budget, id -> id==trueMax, trueMaxVal, message);
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		int i = 0;
		for (String name: finderNames) {
			buf.append("** ").append(name).append("\n");
			buf.append("Percentage of misidentification: ").append(100 * errorCount[i] / (double) epochCount.get()).append("\n");
			buf.append("Amount of rel. error: ").append(errorAmount[i] / errorCount[i]).append("\n");
			buf.append("Average # of samples: ").append(utilization[i] / (double) epochCount.get()).append("\n");
			i++;
		}
		return buf.toString();
	}
}
