package mab.util;

import mab.MaxFinder;
import mab.Result;
import mab.sampler.Sampler;
import mab.util.Moments;
import mab.finder.CheatingFinder;

import java.util.List;
import java.util.function.IntPredicate;

public class FinderComparison {

    private final MaxFinder[] finders;
    private final double[] parameters;
    private String[] finderNames;
    private double[] errorCount, errorAmount;
    private long[] utilization;
    private int epochCount;
    private boolean verbose = true;
    private int size = -1;

    public FinderComparison(MaxFinder[] finders) {
	this(finders, null);
    }

    public FinderComparison(MaxFinder[] finders, double[] parameters) {
		this.size = finders.length;
		if (parameters != null && parameters.length != size)
			throw new IllegalArgumentException("Wrong number of parameters");
		this.finders = finders;
		this.parameters = parameters;
		finderNames = new String[size];
		int i = 0;
		for (MaxFinder finder: finders) {
		    finderNames[i++] = finder.getTag();
		}
		errorCount = new double[size];
		errorAmount = new double[size];
		utilization = new long[size];
    }

    public void noDetailedOutput() {
		verbose = false;
    }

    public void addComparison(List<Sampler> arms, List<Moments> moments, int budget,
			      IntPredicate isTrueMax,
			      double trueMaxVal, String message) {
		int i = 0;
		long start, stop;
		epochCount++;

		for (MaxFinder finder: finders) {
		    if (moments!=null && finder instanceof CheatingFinder) {
			CheatingFinder cf = (CheatingFinder) finder;
			cf.setGroundTruth(moments);
		    }
			start = System.currentTimeMillis();
			Result res = finder.compute(arms, budget);
			stop = System.currentTimeMillis();
			long timeMillis = stop - start;

			int error = 0;
			double relError = 0;
			int max = res.maxIndex;
			double regret = res.totalSampleCount * trueMaxVal - res.totalReward;
			if (!isTrueMax.test(max)) {
				error = 1;
				errorCount[i]++;
				relError = Math.abs(res.maxValue - trueMaxVal) / trueMaxVal;
				errorAmount[i] += relError;
			}
			utilization[i] += res.totalSampleCount;
			if (verbose) {
				StringBuilder sb = new StringBuilder();
				sb.append(finder.getTag()).append("\t");
				if (parameters != null)
					sb.append(parameters[i]).append("\t");
				sb.append(timeMillis).append("\t");
				sb.append(error).append("\t");
				sb.append(relError).append("\t");
				sb.append(regret).append("\t");
				sb.append(res.allocationEntropy());
				sb.append(message);
				System.out.println(sb);
			}
			i++;
		}
	}
    
    
    public void addComparison(List<Sampler> arms, List<Moments> moments, int budget, int trueMax, double trueMaxVal, String message)
	{
	    addComparison(arms, moments, budget, id -> id==trueMax, trueMaxVal, message);
	}

    public void addComparison(List<Sampler> arms, int budget, int trueMax, double trueMaxVal, String message)
	{
	    addComparison(arms, null, budget, id -> id==trueMax, trueMaxVal, message);
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();

		int i = 0;
		for (String name: finderNames) {
			buf.append("** ").append(name).append("\n");
			buf.append("Percentage of misidentification: ").append(100 * errorCount[i] / (double) epochCount).append("\n");
			buf.append("Amount of rel. error: ").append(errorAmount[i] / errorCount[i]).append("\n");
			buf.append("Average # of samples: ").append(utilization[i] / (double) epochCount).append("\n");
			i++;
		}

		return buf.toString();
	}
}
