package mab.util;

import integrate.Integrator;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.integration.UnivariateIntegrator;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.exception.TooManyEvaluationsException;

import java.util.List;

public class DistributionOfMax {
    private UnivariateIntegrator integrator;
    // Used by integrator
    private static final double REL_ACCURACY = 1E-3, ABS_ACCURACY = 1E-4;
    private static final int MAX_ITER = 10000;

    public DistributionOfMax(Integrator integratorType) {
        integrator = integratorType.make(REL_ACCURACY, ABS_ACCURACY);
    }

    public double[] get(NormalDistribution[] normals) {
        double[] probabilities = new double[normals.length];

        for (int index = 0; index < probabilities.length; index++) {
            // NEW: set integration interval per arm
            final double lower = normals[index].getMean() - 3 * normals[index].getStandardDeviation(),
                    upper = normals[index].getMean() + 3 * normals[index].getStandardDeviation();
            try {
                probabilities[index] = integrator.integrate(MAX_ITER, makeFastIntegrand(normals, index), lower, upper);
            } catch (TooManyEvaluationsException e) {
                System.err.println("Warning: maximum iterations exceeded during integration");
                probabilities[index] = 0;
            }
        }
        return probabilities;
    }

    private boolean checkArgs(NormalDistribution[] normals, List<Integer> nullVariance, double[] meansWithNullVariance) {
        if (normals.length != meansWithNullVariance.length)
            return false;
        for (int i=0; i<normals.length; i++) {
            if (normals[i]==null && !nullVariance.contains(i))
                return false;
            if (normals[i]!=null && nullVariance.contains(i))
                return false;
        }
        return true;
    }

    public double[] get(NormalDistribution[] normals, List<Integer> nullVariance, double[] meansWithNullVariance) {
        assert(checkArgs(normals, nullVariance, meansWithNullVariance));

        double[] probabilities = new double[normals.length];
        // calcolare i valori medi minimi e massimi degli arm in nullVariance (Min,Max)
        double min, max;
        min = Double.POSITIVE_INFINITY;
        max = Double.NEGATIVE_INFINITY;
        for (int i: nullVariance) {
            double mean = meansWithNullVariance[i];
            if (mean < min) min = mean;
            if (mean > max) max = mean;
        }

        // calcolare le probabilità degli arm in nullVariance (4)
        for (int i: nullVariance) {
            double prob = 1;
            double mean = meansWithNullVariance[i];
            for (int j=0; j<normals.length; j++) {
                if (j==i) continue;
                if (normals[j] != null) {
                    prob *= NormalCumulativeProbability.eval(normals[j].getMean(),
                            normals[j].getStandardDeviation(), mean);
                } else {
                    prob *= unitStepFunction(meansWithNullVariance[j], mean);
                }
            }
            probabilities[i] = prob;
        }

        //calcolare le probabilità degli altri arm (i)

        //se i.mean + 3i.sigma < Min allora P(i)=0
        //altrimenti a = max(i.mean - 3i.sigma , Max)

        for (int index = 0; index < normals.length; index++) {
            if (normals[index] != null) {
                double a = normals[index].getMean() - 3 * normals[index].getStandardDeviation();
                double b = normals[index].getMean() + 3 * normals[index].getStandardDeviation();
                a = Math.max(a, max);
                if (a>=b) {
                    probabilities[index]=0;
                } else {
                    try {
                        probabilities[index] = integrator.integrate(MAX_ITER, makeFastIntegrand(normals, index), a, b);
                    } catch (TooManyEvaluationsException e) {
                        System.err.println("Warning: maximum iterations exceeded during integration");
                        probabilities[index] = 0;
                    }
                }
            }
        }
        return probabilities;
    }

    private static int unitStepFunction(double threshold, double value){
        if (value >= threshold) return 1;
        else return 0;
    }

    // Slow but precise
    public static UnivariateFunction makePreciseIntegrand(final NormalDistribution[] normals, final int index) {
        return x -> {
            double prod = 1.0;
            for (int j = 0; j<normals.length;j++) {
                if (j!=index && normals[j] != null) {
                    double cumulative = normals[j].cumulativeProbability(x);
                    // System.err.println("precise: " + cumulative);
                    prod *= cumulative;
                }
            }
            return prod * normals[index].density(x);
        };
    }

    // Fast and less precise
    public static UnivariateFunction makeFastIntegrand(final NormalDistribution[] normals, final int index) {
        return x -> {
            double prod = 1.0;
            for (int j = 0; j<normals.length;j++) {
                if (j!=index && normals[j] != null) {
                    double cumulative = NormalCumulativeProbability.eval(
                            normals[j].getMean(), normals[j].getStandardDeviation(), x);
                    // System.err.println("fast: " + cumulative);
                    prod *= cumulative;
                }
            }
            return prod * normals[index].density(x);
        };
    }
}
