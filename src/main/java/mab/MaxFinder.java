package mab;

import mab.sampler.Sampler;
import java.util.List;

public interface MaxFinder {
    /**
     @param budget The maximum number of samples that can be requested over all functions.
     */
    Result compute(List<Sampler> arms, int budget);
    String getTag();
}
