package exp;
import mab.MaxFinder;
import mab.finder.*;
import mab.sampler.Sampler;
import mab.util.FinderComparison;
import mab.util.Moments;

import java.io.PrintStream;
import java.util.*;

/**
 * Performs the FastBR experiments.
 *
 * To run from command line:
 *    mvn exec:java -Dexec.args="results.out" -Dexec.mainClass="exp.FastBRTest"
 */


public class FastBRTest {

    private static final Random random = new Random();
    private static int iterations = 0;

    private static final double SMALL_MIN_SD = 0.01, SMALL_MAX_SD = 0.1,
            MEDIUM_MIN_SD = 0.1, MEDIUM_MAX_SD = 0.5,
            LARGE_MIN_SD = 0.5, LARGE_MAX_SD = 1.5;

    private static void printUsage() {
        System.out.println("Usage: FastBRTest [<output filename>]");
    }

    public static void main( String[] args ) throws java.io.IOException
    {


        if (args.length==0) {
            printUsage();
            return;
        }


        PrintStream outfile = new PrintStream(args[0]);
        System.setOut(outfile);


        final int[] numerositiesFix = { 10 },
                budgetVar = { 200, 400, 600, 800 },
                budgetFix = { 500};

        // Repetitions of the experiment
        iterations = 20_000;

        testOnGenerator(Generators.randomBernoulli(), numerositiesFix, budgetVar);

    }

    public static void testOnGenerator(Generator generator, int[] numerosities, int[] sampleCounts) {
        System.out.println("Iterations: " + iterations);
        System.out.println("Algo\tTime\tError\tRelErr\tRegret\tFNum\tBudget\tGamma\tH1\tH2\tHsigma");

        int n = numerosities.length > sampleCounts.length? numerosities.length : sampleCounts.length;
        for (int k=0; k<n; k++) {
            int numerosity, maxSamples;
            if (numerosities.length > 1) {
                numerosity = numerosities[k];
            } else {
                numerosity = numerosities[0];
            }
            if (sampleCounts.length > 1) {
                maxSamples = sampleCounts[k];
            } else {
                maxSamples = sampleCounts[0];
            }
            String basicMessage = "\t" + numerosity + "\t" + maxSamples;

            for (int attempt=0; attempt<iterations; attempt++) {
                // Populate functions
                if(attempt%1000==0)
                    System.err.println(attempt);

                MaxFinder[] finders = {

                        new Proportional((maxSamples-2*numerosity)+1),
                        //new FastProportional(10),
                        //new FastProportional(20),
                        new FastProportional(30),
                        new UnivThompsonSampling()
                };
                FinderComparison comp = new FinderComparison(finders);

                List<Sampler> samplers = new ArrayList<>(numerosity);
                List<Moments> moments = new ArrayList<>(numerosity);
                int maxId = generator.generate(numerosity, samplers, moments);
                double maxMean = moments.get(maxId).mean;

                // Input statistics
                List<Moments> sortedMoments = new ArrayList<>(moments);
                sortedMoments.sort(Comparator.comparingDouble(m -> m.mean));
                // The closest to the top
                double deltaStar = maxMean - sortedMoments.get(numerosity-2).mean;
                double sigma1squared = sortedMoments.get(numerosity-1).sd * sortedMoments.get(numerosity-1).sd;
                double H1 = 1/(deltaStar*deltaStar),
                        H2 = 1/(deltaStar*deltaStar),
                        Hsigma = Double.NEGATIVE_INFINITY;
                // This loop skips the top
                for (int i=0; i<numerosity-1; i++) {
                    double delta = maxMean - sortedMoments.get(i).mean;
                    H1 += 1/(delta*delta);
                    double term = (numerosity - i) / (delta*delta);
                    double sigmaTerm = (sigma1squared +  sortedMoments.get(i).sd*sortedMoments.get(i).sd ) /
                            (delta*delta);
                    if (term > H2)
                        H2 = term;
                    if (sigmaTerm > Hsigma)
                        Hsigma = sigmaTerm;
                }
                String message = basicMessage + "\t" + H1 + "\t" + H2 + "\t" + Hsigma;

                comp.addComparison(samplers, maxSamples, maxId, maxMean, message);
            }


        }
    }

}
