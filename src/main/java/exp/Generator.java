package exp;

import java.util.*;

import mab.sampler.Sampler;
import mab.util.Moments;

public interface Generator {
	/** Generates a multi-armed bandit problem instance.
	 *
 	 * @param numerosity The number of arms to generate
	 * @param samplers An empty list of samplers (arms) that will be filled in by this method (output parameter)
	 * @param moments An empty list of moments (average and stddev) that will be filled in by this method (output parameter)
	 * @return The index of the best arm in the list of arms <code>samplers</code>
	 */
    int generate(int numerosity,
		 List<Sampler> samplers,
		 List<Moments> moments);
}

    
