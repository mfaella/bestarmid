package exp;

import mab.MaxFinder;
import mab.finder.*;
import mab.sampler.Sampler;
import mab.util.FinderComparison;
import mab.util.Moments;

import java.io.PrintStream;
import java.util.*;

/**
 * Performs the new BAI experiments.
 *
 * To run from command line:
 *    mvn exec:java -Dexec.args="medium entropy.out" -Dexec.mainClass="exp.NewBAIMarco" 
 */
public class NewBAIMarco
{
	private static final Random random = new Random();
	private static int iterations = 0;

	private static final double
	    SMALL_MIN_SD = 0.01, SMALL_MAX_SD = 0.1,
	    MEDIUM_MIN_SD = 0.1, MEDIUM_MAX_SD = 0.5,
	    LARGE_MIN_SD = 0.5, LARGE_MAX_SD = 1.5,
	    SMALL_MIN_ALPHA=1, SMALL_MAX_ALPHA=2,
	    MEDIUM_MIN_ALPHA=5, MEDIUM_MAX_ALPHA=10,
	    LARGE_MIN_ALPHA=10, LARGE_MAX_ALPHA=20;


	private static void printUsage() {
		System.out.println("Usage: bai [rade|bern|small|medium|large|numerosity|H2|mult] [<output filename>]");
	}

	public static void main( String[] args ) throws java.io.IOException
	{
		if (args.length==0) {
			printUsage();
			return;
		}

		if (args.length>1) {
			PrintStream outfile = new PrintStream(args[1]);
			System.setOut(outfile);
		}

		final int[] numerositiesFix = { 40 },
				budgetVar = { 4000, 8000, 12000 },
				budgetFix = { 500 };

		// Repetitions of the experiment (on different random problems)
		iterations = 10_000;

		switch (args[0]) {
			case "rade":
				System.out.println("Random Rademachers.");
				testOnGenerator(Generators.randomTwoValued(), numerositiesFix, budgetVar);
				break;
			case "bern":
				System.out.println("Random Bernoullis.");
				testOnGenerator(Generators.randomBernoulli(), numerositiesFix, budgetVar);
				break;
			case "small":
				System.out.println("Random univariate Gaussians.");
				testOnGenerator(Generators.randomGaussians(SMALL_MIN_SD, SMALL_MAX_SD),
						numerositiesFix, budgetVar);
				break;
			case "medium":
				System.out.println("Random univariate Gaussians.");
				testOnGenerator(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD),
						numerositiesFix, budgetVar);
				break;
			case "large":
				System.out.println("Random univariate Gaussians.");
				testOnGenerator(Generators.randomGaussians(LARGE_MIN_SD, LARGE_MAX_SD),
						numerositiesFix, budgetVar);
				break;
			case "special":
				System.out.println("Random univariate Gaussians with very heterogeneous standard deviations.");
				testOnGenerator(Generators.randomGaussiansWithHeavyTailedStddev(0.1, 1.5),
						numerositiesFix, budgetVar);
				break;
			case "smallp":
				System.out.println("Random Pareto, small alpha.");
				testOnGenerator(Generators.randomPareto(SMALL_MIN_ALPHA, SMALL_MAX_ALPHA),
						numerositiesFix, budgetVar);
				break;
			case "mediump":
				System.out.println("Random Pareto, medium alpha.");
				testOnGenerator(Generators.randomPareto(MEDIUM_MIN_ALPHA, MEDIUM_MAX_ALPHA),
						numerositiesFix, budgetVar);
				break;
			case "largep":
				System.out.println("Random Pareto, large alpha.");
				testOnGenerator(Generators.randomPareto(SMALL_MIN_ALPHA, SMALL_MAX_ALPHA),
						numerositiesFix, budgetVar);
				break;
			case "numerosity":
				int[] numerositiesVar = { 20, 40, 80, 160, 320 }, budgets = { 2000, 4000, 8000, 16000, 32000 };
				System.out.println("Random Bernoullis with varying number of arms.");
				if (args.length>2) {
					int k = Integer.valueOf(args[2]);
					testOnGenerator(Generators.randomBernoulli(),
							Arrays.copyOfRange(numerositiesVar, k, k+1),
							Arrays.copyOfRange(budgets, k, k+1));
				} else {
					testOnGenerator(Generators.randomBernoulli(), numerositiesVar, budgets);
				}
				// Old version: on Gaussians with medium variance
				// System.out.println("Random univariate Gaussians.");
				// testOnGenerator(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD), numerositiesVar, budgets);
				break;
			case "H2":
				System.out.println("Random fixed-H2 Gaussians.");
				testOnGenerator(Generators.fixedH2(), numerositiesFix, budgetVar);
				break;
			default:
				System.out.println("Unrecognized option.");
				printUsage();
		}
	}

	public static void testOnGenerator(Generator generator, int[] numerosities, int[] sampleCounts) {
		final double sdMultiplier = 2;

		System.out.println("Iterations: " + iterations);
		System.out.println("Algo\tTime\tError\tRelErr\tRegret\tEntropy\tFNum\tBudget\tGamma\tH1\tH2\tHsigma");
		MaxFinder[] finders = {
		    // new SuccessiveRejects(),
		    new SequentialHalving(),
		    new SHAdaVar(0.05),
				//new AdaptiveUCBE(1.0),
				//new GapEVMaxFinder(1.0),
		    //new Uniform(),
		    SmartMaxFinder.newFixedBudgetIid(sdMultiplier),
				// new UnivThompsonSampling()
		    //new VBR(0.05, VBR.UncertaintyType.EMP_VARIANCE, VBR.SelectionCriterion.ROUND_ROBIN),
			//new VBR(0.05, VBR.UncertaintyType.TRUE_VARIANCE, VBR.SelectionCriterion.ROUND_ROBIN),
			//new VBR(0.05, VBR.UncertaintyType.LALITHA, VBR.SelectionCriterion.ROUND_ROBIN),
			// new VBR(0.05, VBR.UncertaintyType.LALITHA, VBR.SelectionCriterion.MAX_UNCERTAINTY),
			new VBR(0.05, VBR.UncertaintyType.EMP_VARIANCE, VBR.SelectionCriterion.ROUND_ROBIN),
			// new VBR(20, VBR.UncertaintyType.EMP_VARIANCE, VBR.SelectionCriterion.ROUND_ROBIN),
			// new VBR(30, VBR.UncertaintyType.EMP_VARIANCE, VBR.SelectionCriterion.ROUND_ROBIN),
			//new VBR(20, VBR.UncertaintyType.LALITHA, VBR.SelectionCriterion.ROUND_ROBIN),
			//new VBR(30, VBR.UncertaintyType.LALITHA, VBR.SelectionCriterion.ROUND_ROBIN),
			new VBR(0.05, VBR.UncertaintyType.LALITHA, VBR.SelectionCriterion.ROUND_ROBIN),
				// new CheatingMaxFinder(),
				// new Proportional(20),
				// new FastProportional(30),
		        // new OptimalMaxFinder(samplers, functions, moments, maxSamples),
				// new Entropy(samplers, functions, maxSamples),
				// new Boltzmann2MaxFinder(0.1),
				// new BoltzmannDoneRight(0.1)
		};
		FinderComparison comp = new FinderComparison(finders);

		int n = numerosities.length > sampleCounts.length? numerosities.length : sampleCounts.length;
		// System.err.println("sc:"+sampleCounts.length);
		for (int k=0; k<n; k++) {
		    int numerosity, maxSamples;
		    if (numerosities.length > 1) {
			numerosity = numerosities[k];
		    } else {
			numerosity = numerosities[0];
		    }
		    if (sampleCounts.length > 1) {
			maxSamples = sampleCounts[k];
		    } else {
			maxSamples = sampleCounts[0];
		    }
		    System.err.println("budget: "+sampleCounts[k]);
		    String basicMessage = "\t" + numerosity + "\t" + maxSamples + "\t" + sdMultiplier;
		    
		    for (int attempt=0; attempt<iterations; attempt++) {
				// Populate functions
				if(attempt%1000==0)
					System.err.println(attempt);
				List<Sampler> samplers = new ArrayList<>(numerosity);
				List<Moments> moments = new ArrayList<>(numerosity);
				int maxId = generator.generate(numerosity, samplers, moments);
				double maxMean = moments.get(maxId).mean;

				// Input statistics
				List<Moments> sortedMoments = new ArrayList<>(moments);
				sortedMoments.sort(Comparator.comparingDouble(m -> m.mean));
				// The closest to the top
				double deltaStar = maxMean - sortedMoments.get(numerosity-2).mean;
				double sigma1squared = sortedMoments.get(numerosity-1).sd * sortedMoments.get(numerosity-1).sd;
				double H1 = 1/(deltaStar*deltaStar),
					   H2 = 1/(deltaStar*deltaStar),
						Hsigma = Double.NEGATIVE_INFINITY;
				// This loop skips the top
				for (int i=0; i<numerosity-1; i++) {
					double delta = maxMean - sortedMoments.get(i).mean;
					H1 += 1/(delta*delta);
					double term = (numerosity - i) / (delta*delta);
					double sigmaTerm = (sigma1squared +  sortedMoments.get(i).sd*sortedMoments.get(i).sd ) /
							(delta*delta);
					if (term > H2)
						H2 = term;
					if (sigmaTerm > Hsigma)
						Hsigma = sigmaTerm;
				}
				String message = basicMessage + "\t" + H1 + "\t" + H2 + "\t" + Hsigma;

				comp.addComparison(samplers, moments, maxSamples, maxId, maxMean, message);
			}
		}
	}
}
