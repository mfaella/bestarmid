package exp;

import mab.MaxFinder;
import mab.Result;
import mab.finder.*;
import mab.sampler.Sampler;
import mab.util.FinderComparison;
import mab.util.Moments;

import java.util.*;

/**
 * A main function useful for debugging specific solvers.
 *
 */
public class DebuggingHR
{
	private static final double SMALL_MIN_SD = 0.01, SMALL_MAX_SD = 0.1,
			MEDIUM_MIN_SD = 0.1, MEDIUM_MAX_SD = 0.5,
			LARGE_MIN_SD = 0.5, LARGE_MAX_SD = 1.5;

	public static void main( String[] args ) throws java.io.IOException
	{
		final int[] numerositiesFix = { 435, 40, 128, 127, 129, 50, 70 }, budgetVar = { 2175, 5000, 1000, 15000, 15000, 15000, 15000 };

		testOnGenerator(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD),
				numerositiesFix, budgetVar);
	}

	public static void testOnGenerator(Generator generator, int[] numerosities, int[] sampleCounts) {
		final double sdMultiplier = 2;

		FinderComparison comp = new FinderComparison(null);
		System.out.println("Algo\tTime\tError\tRelErr\tFNum\tBudget\tGamma\tH1\tH2\tHsigma");

		int iterations = 1;

		int n = numerosities.length > sampleCounts.length? numerosities.length : sampleCounts.length;
		for (int k=0; k<n; k++) {
			int numerosity, maxSamples;
			if (numerosities.length > 1) {
				numerosity = numerosities[k];
			} else {
				numerosity = numerosities[0];
			}
			if (sampleCounts.length > 1) {
				maxSamples = sampleCounts[k];
			} else {
				maxSamples = sampleCounts[0];
			}
			String basicMessage = "\t" + numerosity + "\t" + maxSamples + "\t" + sdMultiplier;

			for (int attempt=0; attempt<iterations; attempt++) {
				// Populate functions
				List<Sampler> samplers = new ArrayList<>(numerosity);
				List<Moments> moments = new ArrayList<>(numerosity);

				int maxId = generator.generate(numerosity, samplers, moments);

				// This loop skips the top
				MaxFinder maxFinder = new SequentialHalving();
				Result res = maxFinder.compute(samplers, maxSamples);
				System.out.println("effective budget: " + res.totalSampleCount);
				// comp.addComparison(maxId, maxMean, message, proportional);
			}
		}
	}
}
