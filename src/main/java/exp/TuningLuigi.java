package exp;

import mab.MaxFinder;
import mab.finder.*;
import mab.sampler.Sampler;
import mab.util.FinderComparison;
import mab.util.Moments;

import java.io.PrintStream;
import java.util.*;
import java.util.function.DoubleFunction;

/**
 * Performs the new BAI experiments.
 *
 * To run from command line:
 *    mvn exec:java -Dexec.args="medium entropy.out" -Dexec.mainClass="exp.NewBAI" 
 */
public class TuningLuigi
{
	private static int iterations = 0;

	private static final double SMALL_MIN_SD = 0.01, SMALL_MAX_SD = 0.1,
			MEDIUM_MIN_SD = 0.1, MEDIUM_MAX_SD = 0.5,
			LARGE_MIN_SD = 0.5, LARGE_MAX_SD = 1.5;

	private static void printUsage() {
		System.out.println("Usage: tune {rade|bern|small|medium|large} <output filename> <min> <max> <multiplier>");
		System.out.println("Tunes a single-parameter MAB algorithm, trying all values from min to max," +
				"with update function p := p * multiplier. Multiplier must be greater than 1.");
	}

	public static void main( String[] args ) throws java.io.IOException
	{
		if (args.length < 5) {
			printUsage();
			return;
		}

		PrintStream outfile = new PrintStream(args[1]);
		System.setOut(outfile);
		double minParam = Double.parseDouble(args[2]);
		double maxParam = Double.parseDouble(args[3]);
		double multiplier = Double.parseDouble(args[4]);

		if (minParam > maxParam)
			throw new IllegalArgumentException("min cannot be greater than max");
		if (multiplier<=1.0)
			throw new IllegalArgumentException("multiplier must be greater than 1");

		int[] numerositiesFix = { 50 },
				budgetVar = { 500, 1000, 2000, 5000 },
				budgetFix = { 500 };

		// Repetitions of the experiment
		iterations = 10_000;

		switch (args[0]) {
			case "bern":
				System.out.println("Random Bernoullis.");
				tuneOnGenerator(Generators.randomBernoulli(),
						numerositiesFix, budgetFix,	minParam, maxParam, multiplier);
				break;
			case "small":
				numerositiesFix = new int[] { 40 };
				budgetFix = new int[] { 500 };

				System.out.println("Random univariate Gaussians with small variance.");
				tuneOnGenerator(Generators.randomGaussians(SMALL_MIN_SD, SMALL_MAX_SD),
						numerositiesFix, budgetFix, minParam, maxParam, multiplier);
				break;
			case "medium":
				System.out.println("Random univariate Gaussians with medium variance.");
				tuneOnGenerator(Generators.randomGaussians(MEDIUM_MIN_SD, MEDIUM_MAX_SD),
						numerositiesFix, budgetFix, minParam, maxParam, multiplier);
				break;
			case "large":
				System.out.println("Random univariate Gaussians with large variance.");
				tuneOnGenerator(Generators.randomGaussians(LARGE_MIN_SD, LARGE_MAX_SD),
						numerositiesFix, budgetFix, minParam, maxParam, multiplier);
				break;
			default:
				System.out.println("Unrecognized option.");
				printUsage();
		}
	}

	public static void tuneOnGenerator(Generator generator, int[] numerosities, int[] sampleCounts,
									   double minParam, double maxParam, double multiplier) {

		// Prepare finders
		DoubleFunction<MaxFinder>[] parametricSuppliers =
				new DoubleFunction[] { x -> new Boltzmann2MaxFinder(x),
						x -> new BoltzmannDoneRight(x) };
		MaxFinder[] baselines = new MaxFinder[] { new Uniform(), new FastProportional(20)};//, new FastProportionalBoltzmannGumbelMaxFinder(20) };

		int nparams = (int) Math.ceil(Math.log(maxParam / minParam) / Math.log(multiplier));
		int nBaselines = baselines.length;
		int nParametric = parametricSuppliers.length;
		MaxFinder[] finders = new MaxFinder[nParametric * nparams + nBaselines];
		double[] parameters = new double[nParametric * nparams + nBaselines];
		// Baseline algorithms
		int j = 0;
		for (MaxFinder finder : baselines) {
			finders[j] = finder;
			parameters[j] = Double.NaN;
			j++;
		}
		for (DoubleFunction<MaxFinder> parametricSupplier : parametricSuppliers) {
			for (double p = minParam; p < maxParam; p *= multiplier) {
				finders[j] = parametricSupplier.apply(p);
				parameters[j] = p;
				j++;
			}
		}

		// Perform experiment
		System.out.println("Iterations: " + iterations);
		System.out.println("Algo\tParam\tTime\tError\tRelErr\tRegret\tFNum\tBudget\tGamma\tH1\tH2\tHsigma");

		int n = Math.max(numerosities.length, sampleCounts.length);
		for (int k=0; k<n; k++) {
			int numerosity, maxSamples;
			if (numerosities.length > 1) {
				numerosity = numerosities[k];
			} else {
				numerosity = numerosities[0];
			}
			if (sampleCounts.length > 1) {
				maxSamples = sampleCounts[k];
			} else {
				maxSamples = sampleCounts[0];
			}
			String basicMessage = "\t" + numerosity + "\t" + maxSamples;
			FinderComparison comp = new FinderComparison(finders);

			for (int attempt=0; attempt<iterations; attempt++) {
				// Populate functions
				List<Sampler> samplers = new ArrayList<>(numerosity);
				List<Moments> moments = new ArrayList<>(numerosity);

				int maxId = generator.generate(numerosity, samplers, moments);
				double maxMean = moments.get(maxId).mean;

				// Input statistics
				List<Moments> sortedMoments = new ArrayList<>(moments);
				sortedMoments.sort(Comparator.comparingDouble(m -> m.mean));
				// The closest to the top
				double deltaStar = maxMean - sortedMoments.get(numerosity-2).mean;
				double sigma1squared = sortedMoments.get(numerosity-1).sd * sortedMoments.get(numerosity-1).sd;
				double H1 = 1/(deltaStar*deltaStar),
					   H2 = 1/(deltaStar*deltaStar),
						Hsigma = Double.NEGATIVE_INFINITY;
				// This loop skips the top
				for (int i=0; i<numerosity-1; i++) {
					double delta = maxMean - sortedMoments.get(i).mean;
					H1 += 1/(delta*delta);
					double term = (numerosity - i) / (delta*delta);
					double sigmaTerm = (sigma1squared +  sortedMoments.get(i).sd*sortedMoments.get(i).sd ) /
							(delta*delta);
					if (term > H2)
						H2 = term;
					if (sigmaTerm > Hsigma)
						Hsigma = sigmaTerm;
				}
				String message = basicMessage + "\t" + H1 + "\t" + H2 + "\t" + Hsigma;
				comp.addComparison(samplers, maxSamples, maxId, maxMean, message);
			}
		}
	}
}
