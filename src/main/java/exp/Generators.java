package exp;

import java.util.*;

import mab.sampler.ApacheSampler;
import mab.sampler.Sampler;
import mab.util.Moments;
import org.apache.commons.math3.distribution.*;
import org.apache.commons.math3.random.Well19937c;

public class Generators {

    private static final Random random = new Random();

	public static Generator randomPareto(final double minAlpha, final double maxAlpha) {
		return (int numerosity, List<Sampler> samplers, List<Moments> moments) ->
		{
			int maxId = 0;
			double maxMean = Double.NEGATIVE_INFINITY;

			for (int i=0; i<numerosity; i++) {
				double alpha=minAlpha+(maxAlpha-minAlpha)*random.nextDouble();
                ParetoDistribution pd= new ParetoDistribution(1, alpha);

				Moments m=new Moments();
				m.mean=pd.getNumericalMean();
				m.sd=Math.sqrt(pd.getNumericalVariance());
                moments.add(m);
				if (m.mean > maxMean) {
					maxMean = m.mean;
					maxId = i;
				}
				samplers.add(new ApacheSampler(pd));
			}
			//System.err.println("maxId:"+maxId);
			return maxId;
		};
	}


    public static Generator randomLogGaussians(final double minSd, final double maxSd) {
        return (int numerosity, List<Sampler> samplers, List<Moments> moments) ->
        {
            final double MIN_MEAN = 0, MAX_MEAN = 1;

            int maxId = 0;
            double maxMean = Double.NEGATIVE_INFINITY;

            for (int i=0; i<numerosity; i++) {
                Moments m = new Moments();
                double mean=MIN_MEAN + (MAX_MEAN - MIN_MEAN) * random.nextDouble();
                double sd=minSd + (maxSd - minSd) * random.nextDouble();
                m.mean = Math.exp(mean+((sd*sd)/2));
                m.sd   = Math.exp(2*mean+(sd*sd))*(Math.exp(sd*sd)-1);
                moments.add(m);

                samplers.add(new ApacheSampler(new LogNormalDistribution(mean, sd)));
                if (m.mean > maxMean) {
                    maxMean = m.mean;
                    maxId = i;
                }
            }
            return maxId;
        };
    }

	/**
	 * Returns a generator that returns <code>n</code> Gaussian distributions whose means
	 * are uniformly distributed in [0, 1] and whose stddevs
	 * are uniformly distributes in [minSd, maxSd].*
	 */
	public static Generator randomGaussians(final double minSd, final double maxSd) {
		Well19937c randomGenerator=new Well19937c();
		return (int numerosity, List<Sampler> samplers, List<Moments> moments) ->
		{
		    final double MIN_MEAN = 0, MAX_MEAN = 1;

			int maxId = 0;
			double maxMean = Double.NEGATIVE_INFINITY;

			for (int i=0; i<numerosity; i++) {
				Moments m = new Moments();
				m.mean = MIN_MEAN + (MAX_MEAN - MIN_MEAN) * random.nextDouble();
				m.sd   = minSd + (maxSd - minSd) * random.nextDouble();
				moments.add(m);

				samplers.add(new ApacheSampler(new NormalDistribution(randomGenerator, m.mean, m.sd)));
				if (m.mean > maxMean) {
					maxMean = m.mean;
					maxId = i;
				}
			}
			return maxId;
		};
	}

	/**
	 * Returns a generator that returns <code>n</code> Gaussian distributions whose means
	 * are uniformly distributed in [0, 1] and whose stddevs
	 * are sampled from a Pareto distribution of given scale and shape.
	 *
	 * @param scale The scale of the Pareto distribution used to sample the stddevs
	 * @param shape The shape of the Pareto distribution used to sample the stddevs
	 * @return A generator Gaussians whose stddevs are heavy tailed
	 */
	public static Generator randomGaussiansWithHeavyTailedStddev(double scale, double shape) {
		return (int numerosity, List<Sampler> samplers, List<Moments> moments) ->
		{
			final double MIN_MEAN = 0, MAX_MEAN = 1;
			int maxId = 0;
			double maxMean = Double.NEGATIVE_INFINITY;
			RealDistribution stddevGenerator = new ParetoDistribution(scale, shape);

			for (int i=0; i<numerosity; i++) {
				Moments m = new Moments();
				m.mean = MIN_MEAN + (MAX_MEAN - MIN_MEAN) * random.nextDouble();
				m.sd   = stddevGenerator.sample();
				moments.add(m);
				samplers.add(new ApacheSampler(new NormalDistribution(m.mean, m.sd)));
				if (m.mean > maxMean) {
					maxMean = m.mean;
					maxId = i;
				}
			}
			return maxId;
		};
	}

	/**
	 * A generator of two bernulli distributions,
	 * one for the best arm and the other for all the remaining arms.
	 */

	public static Generator HardBernoulli(double maxp, double minp) {
		return (int numerosity,	List<Sampler> samplers, List<Moments> moments) ->
		{
			if(maxp<=minp) throw new RuntimeException("Invalid inputs");
			double[] values = { 0, 1 }, probs = { 0, 0};
			Moments m = new Moments();
			probs[1] = m.mean = maxp;
			probs[0] = 1 - probs[1];
			m.sd   = Math.sqrt(probs[0] * probs[1]);
			moments.add(m);
			samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));
			double maxMean = m.mean;
			int maxId = 0;

			for (int i=1; i<numerosity; i++) {
				m = new Moments();

				probs[1] = m.mean = minp;
				probs[0] = 1 - probs[1];
				m.sd   = Math.sqrt(probs[0] * probs[1]);
				moments.add(m);

				samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));
//				if (m.mean > maxMean) {	maxMean = m.mean; maxId = i;}
			}
			return maxId;
		};
	}


	public static Generator customAudibertColt10E5Bernulli() {
		return (int numerosity,	List<Sampler> samplers, List<Moments> moments) ->
		{
			if(numerosity!=15) throw new RuntimeException("This generator demands numerosity = 15");
			double[] values = { 0, 1 }, probs = { 0, 0};
			int maxId = 0;

			Moments m = new Moments();
			probs[1]= m.mean = 0.5;
			probs[0] = 1- probs[1];
			m.sd   = Math.sqrt(probs[0] * probs[1]);
			moments.add(m);
			samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));

			for (int i = 1; i<numerosity; i++) {
				m = new Moments();
				probs[1]= m.mean = 0.5 - 0.025 * (i+1);
				probs[0] = 1- probs[1];
				m.sd   = Math.sqrt(probs[0] * probs[1]);
				moments.add(m);
				samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));
			}
			return maxId;
		};
	}


	public static Generator randomBernoulli() {
	return (int numerosity,	List<Sampler> samplers, List<Moments> moments) ->
	    {
		int maxId = 0;
		double maxMean = Double.NEGATIVE_INFINITY;
		double[] values = { 0, 1 }, probs = { 0, 0 };
		
		for (int i=0; i<numerosity; i++) {
		    Moments m = new Moments();

		    probs[1] = m.mean = random.nextDouble();
		    probs[0] = 1 - probs[1];
		    m.sd   = Math.sqrt(probs[0] * probs[1]);
		    moments.add(m);

		    samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));
		    if (m.mean > maxMean) {
			maxMean = m.mean;
			maxId = i;
		    }
		}
		return maxId;
	    };
    }

	/**
	 * A generator of uniform distributions between two (random) values.
	 */
	public static Generator randomTwoValued() {
		final double MIN_VALUE = 0, MAX_VALUE = 1;
		return (int numerosity,	List<Sampler> samplers, List<Moments> moments) ->
		{
			double[] values = new double[2], probs = new double[] { 0.5, 0.5 };
			double maxMean = Double.NEGATIVE_INFINITY;
			int maxId = 0;
			for (int i=0; i<numerosity; i++) {
				values[0] = MIN_VALUE + (MAX_VALUE - MIN_VALUE) * random.nextDouble();
				values[1] = MIN_VALUE + (MAX_VALUE - MIN_VALUE) * random.nextDouble();
				samplers.add(new ApacheSampler(new EnumeratedRealDistribution(values, probs)));
				double mean = (values[0] + values[1]) / 2;
				double sd = (Math.max(values[0], values[1]) - Math.min(values[0], values[1]))/2.0;
				moments.add(new Moments(mean, sd));
				if (mean > maxMean) {
					maxMean = mean;
					maxId = i;
				}
			}
			return maxId;
		};
	}

    public static Generator fixedH2() {
	return (int numerosity,	List<Sampler> samplers, List<Moments> moments) ->
	    {
		final double minSd = 2, maxSd = 10;

		// Top arm
		Moments m1 = new Moments();
		m1.mean = 1;
		m1.sd = minSd + (maxSd - minSd) * random.nextDouble();
		moments.add(m1);
		samplers.add(new ApacheSampler(new NormalDistribution(m1.mean, m1.sd)));

		for (int i=1; i<numerosity; i++) {
		    Moments m = new Moments();
		    m.mean = 0;
		    m.sd   = minSd + (maxSd - minSd) * random.nextDouble();
		    moments.add(m);  
		    samplers.add(new ApacheSampler(new NormalDistribution(m.mean, m.sd)));
		}
		return 0;
	    };
    }

	/**
	 * Returns a generator that returns <code>n</code> Gaussian distributions whose means
	 * are uniformly spaced in the interval <code>[0, width]</code> and whose stddev
	 * are all equal to <code>stddev</code>.
	 * @param width The width of the interval spanned by the means
	 * @param stddev The stddev of each arm
	 * @return A generator of uniformly spaced Gaussians
	 */
	public static Generator regularGaussians(double width, double stddev) {
		return (int numerosity,	List<Sampler> samplers, List<Moments> moments) ->
		{
			for (int i = 0; i<numerosity; i++) {
				Moments m = new Moments();
				m.mean = i * (width/numerosity);
				m.sd   = stddev;
				moments.add(m);
				samplers.add(new ApacheSampler(new NormalDistribution(m.mean, m.sd)));
			}
			return numerosity-1;
		};
	}


}
